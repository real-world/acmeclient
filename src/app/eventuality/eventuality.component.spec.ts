import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventualityComponent } from './eventuality.component';

describe('EventualityComponent', () => {
  let component: EventualityComponent;
  let fixture: ComponentFixture<EventualityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventualityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventualityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
