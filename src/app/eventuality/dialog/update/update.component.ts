import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import { EventualityService } from '../../../services/eventuality.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UpdateComponent>,
       @Inject(MAT_DIALOG_DATA) public data: any, public eventualityService: EventualityService) {
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  getErrorDate() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('date') ? 'Not valid form' :
      '';
  }
  submit() {
    //
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.eventualityService.updateEventuality(this.data);
  }

  ngOnInit() {
  }
}
