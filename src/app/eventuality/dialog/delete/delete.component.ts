import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { EventualityService } from '../../../services/eventuality.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class EvenualityDeleteComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EvenualityDeleteComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public _Service: EventualityService) {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this._Service.deleteEventuality(this.data.id);
  }

  ngOnInit() {
  }

}
