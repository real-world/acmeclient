import {Component, Inject, OnInit} from '@angular/core';
import {Eventuality} from '../../../shared/eventuality';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {EventualityService} from '../../../services/eventuality.service';
import {FormControl, Validators} from '@angular/forms';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class EventualityAddComponent implements OnInit {

  dateEvent: string;
  injuryType: string;
  typeEvents = [
    {value: 1, viewValue: 'Incidente'},
    {value: 2, viewValue: 'Accidente'}
  ];

  injuryTypes = [
    {value: 1, viewValue: 'Escoriaciones'},
    {value: 2, viewValue: 'Heridas punzantes'},
    {value: 3, viewValue: 'Heridas cortantes'},
    {value: 4, viewValue: 'Heridas contusas'},
    {value: 5, viewValue: 'Perdida de tejido'},
    {value: 6, viewValue: 'Contusiones'},
    {value: 7, viewValue: 'Luxacion'},
    {value: 8, viewValue: 'Fractura'},
    {value: 9, viewValue: 'Amputacion'},
    {value: 10, viewValue: 'Quemadura'},
    {value: 11, viewValue: 'Cuerpo extraño en el ojo'},
    {value: 12, viewValue: 'Perdida ocular'},
  ];

  injuryParts = [
    {value: 1, viewValue: 'Region craneana'},
    {value: 2, viewValue: 'Ojos'},
    {value: 3, viewValue: 'Boca'},
    {value: 4, viewValue: 'Cara'},
    {value: 5, viewValue: 'Nariz y senos paranasales'},
    {value: 6, viewValue: 'Aparato auditivo'},
    {value: 7, viewValue: 'Cabeza ubicaciones multiples'},
    {value: 8, viewValue: 'Cuello'},
    {value: 9, viewValue: 'Region cervical'},
    {value: 10, viewValue: 'Region lumbosacra'},
    {value: 11, viewValue: 'Region dorsal'},
    {value: 12, viewValue: 'Torax'},
  ];

  employees = [
    {value: 1, viewValue: 'Miguel Lopez'},
    {value: 2, viewValue: 'Dave Roberts'},
    {value: 3, viewValue: 'Marcelo Equise'},
    {value: 4, viewValue: 'Jorse Posada'},
    {value: 5, viewValue: 'Rubert Alba'},
    {value: 6, viewValue: 'Ivan Paniagua'},
    {value: 7, viewValue: 'Carolina Andrade'},
    {value: 8, viewValue: 'Mario Bross'},
    {value: 9, viewValue: 'Ernesto Cadima'},
    {value: 10, viewValue: 'Pedro Picapiedra'},
    {value: 11, viewValue: 'Pablo Marmol'},
    {value: 12, viewValue: 'Demetrio Angulo'},
    {value: 13, viewValue: 'Claudia Mendieta'},
    {value: 14, viewValue: 'Maria Gumucio'},
    {value: 15, viewValue: 'Juan Perez'},
  ];

  constructor(public dialogRef: MatDialogRef<EventualityAddComponent>,
              @Inject(MAT_DIALOG_DATA) public employeeId: number,
              @Inject(MAT_DIALOG_DATA) public data: Eventuality,
              public eventualityService: EventualityService) {
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.eventualityService.addEventuality(this.data);
  }

  ngOnInit() {
  }

}
