import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/table';
import {UpdateComponent} from '../eventuality/dialog/update/update.component';
import {EventualityAddComponent} from '../eventuality/dialog/add/add.component';
import {EvenualityDeleteComponent} from '../eventuality/dialog/delete/delete.component';
import {EventualityService} from '../services/eventuality.service';
import {Eventuality} from '../shared/eventuality';

import {BehaviorSubject, Observable} from 'rxjs';

@Component({
  selector: 'app-eventuality',
  templateUrl: './eventuality.component.html',
  styleUrls: ['./eventuality.component.scss'],
  providers: [EventualityService],
})
export class EventualityComponent implements OnInit  {
  displayedColumns = ['id', 'type', 'dateEvent', 'injuryType', 'injuryPart',
                      'description', 'employeeId', 'actions'];
  dataBase: EventualityService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public eventuality: EventualityService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  addNew(eventuality: Eventuality) {
    const dialogRef = this.dialog.open(EventualityAddComponent, {
      data: {eventuality: eventuality}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.dataBase.dataChange.value.push(this.eventuality.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }
  startEdit(i: number, id: number, type: string, dateEvent: string, injuryType: string, injuryPart: string,
    description: string, employeeId: number) {
    this.id = id;
    this.index = i;
    const dialogRef = this.dialog.open(UpdateComponent, {
      data: {
        id: id,
        type: type,
        dateEvent: dateEvent,
        injuryType: injuryType,
        injuryPart: injuryPart,
        description: description,
        employeeId: employeeId
      }
    });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 1) {
          const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
          this.dataBase.dataChange.value[foundIndex] = this.eventuality.getDialogData();
          this.loadData();
          this.refreshTable();
        }
      });
  }


  deleteItem(i: number, id: number, type: string, dateEvent: string, injuryType: string, injuryPart: string,
    description: string, employeeId: number) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(EvenualityDeleteComponent, {
      data: {
        id: id,
        type: type,
        dateEvent: dateEvent,
        injuryType: injuryType,
        injuryPart: injuryPart,
        description: description,
        employeeId: employeeId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
          const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
          this.dataBase.dataChange.value.splice(foundIndex, 1);
          this.loadData();
          this.refreshTable();
      }
    });
  }

  private refreshTable() {
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

  public loadData() {
    this.dataBase = new EventualityService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}


export class ExampleDataSource extends DataSource<Eventuality> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Eventuality[] = [];
  renderedData: Eventuality[] = [];

  constructor(public _eventualityData: EventualityService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  connect(): Observable<Eventuality[]> {
    const displayDataChanges = [
      this._eventualityData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    this._eventualityData.getAllEventualities();

    return Observable.merge(...displayDataChanges).map(() => {
      this.filteredData = this._eventualityData.data.slice().filter((eventuality: Eventuality) => {
        if (eventuality !== undefined) {
          const searchStr = (eventuality.id + eventuality.type + eventuality.dateEvent + eventuality.injuryType
                                      + eventuality.injuryPart + eventuality.description + eventuality.employee).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      const sortedData = this.sortData(this.filteredData.slice());

      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }

  sortData(data: Eventuality[]): Eventuality[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'type':
          [propertyA, propertyB] = [a.type, b.type];
          break;
        case 'dateEvent':
          [propertyA, propertyB] = [a.dateEvent, b.dateEvent];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}
