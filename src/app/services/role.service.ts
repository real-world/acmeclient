import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Role} from '../shared/role';
import {baseURL} from '../shared/baseUrl';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class RoleService {
  dataChange: BehaviorSubject<Role[]> = new BehaviorSubject<Role[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private http: HttpClient) { }
  get data(): Role[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }

  getAllRoles(): void {
    this.http.get<Role[]>(baseURL + 'roles').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getRole(id: number): Observable<Role> {
    return this.http.get<Role>(baseURL + 'roles/' + id)
      .map(res => {
        return res;
      });
  }
  addRole(role: Role): void {
    this.http.post(baseURL + 'roles', role).subscribe(data => {
        this.dialogData = role;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateRole(role: Role): void {
    this.http.put(baseURL + 'roles', role).subscribe(data => {
        this.dialogData = role;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
  deleteRole(id: number): void {
    this.http.delete(baseURL + 'roles/' + id).subscribe(data => {
      console.log('Successfully deleted');
    });
  }
}
