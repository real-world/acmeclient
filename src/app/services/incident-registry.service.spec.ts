import {inject, TestBed} from '@angular/core/testing';

import {IncidentRegistryService} from './incident-registry.service';

describe('IncidentRegistryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IncidentRegistryService]
    });
  });

  it('should be created', inject([IncidentRegistryService], (service: IncidentRegistryService) => {
    expect(service).toBeTruthy();
  }));
});
