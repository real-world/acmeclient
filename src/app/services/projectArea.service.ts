import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {baseURL} from '../shared/baseUrl';
//import {ProjectArea} from '../shared/projectArea';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';



@Injectable()
export class ProjectAreaService {
 // dataChange: BehaviorSubject<ProjectArea[]> = new BehaviorSubject<ProjectArea[]>([]);
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  // get data(): ProjectArea[] {
  //   return this.dataChange.value;
  // }

  getDialogData() {
    return this.dialogData;
  }

  addProjectArea(project): void {
    this.http.post(baseURL + 'projectArea', project).subscribe(data => {
        this.dialogData = project;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
}
