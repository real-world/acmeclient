import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Skill} from '../shared/skill';
import {baseURL} from '../shared/baseUrl';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class SkillService {
  dataChange: BehaviorSubject<Skill[]> = new BehaviorSubject<Skill[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private http: HttpClient) { }
  get data(): Skill[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }

  getAllSkills(): void {
    this.http.get<Skill[]>(baseURL + 'skills').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getSkill(id: number): Observable<Skill> {
    return this.http.get<Skill>(baseURL + 'skills/' + id)
      .map(res => {
        return res;
      });
  }
  addSkill(skill: Skill): void {
    this.http.post(baseURL + 'skills', skill).subscribe(data => {
        this.dialogData = skill;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateSkill(skill: Skill): void {
    this.http.put(baseURL + 'skills', skill).subscribe(data => {
        this.dialogData = skill;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
  deleteSkill(id: number): void {
    this.http.delete(baseURL + 'skills/' + id).catch(error => {
      console.log('error' + error);
      return error;
    });
  }
}

