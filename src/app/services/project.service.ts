import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {baseURL} from '../shared/baseUrl';
import {Project} from '../shared/project';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';


@Injectable()
export class ProjectService {
  dataChange: BehaviorSubject<Project[]> = new BehaviorSubject<Project[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): Project[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllProjects(): void {
    this.http.get<Project[]>(baseURL + 'project').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getProject(id: number): Observable<Project> {
    return this.http.get<Project>(baseURL + 'projects/' + id)
      .map(res => {
        return res;
      });
  }
  addProject(project: Project): void {
    this.http.post(baseURL + 'project', project).subscribe(data => {
        this.dialogData = project;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
  updateProject(project: Project): void {
    this.http.put(baseURL + 'project', project).subscribe(data => {
        this.dialogData = project;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  deleteProject(id: number): void {
    this.http.delete(baseURL + 'project/' + id).subscribe(data => {
        console.log('Successfully deleted');
        window.location.reload();
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
}
