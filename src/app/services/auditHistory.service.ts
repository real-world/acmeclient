
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import {BehaviorSubject, Observable} from 'rxjs';
import {baseURL} from '../shared/baseUrl';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {AuditHistory} from '../shared/auditHistory';

@Injectable()
export class AuditHistoryService {dataChange: BehaviorSubject<AuditHistory[]> = new BehaviorSubject<AuditHistory[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): AuditHistory[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAuditHistorys(): void {
    this.http.get<AuditHistory[]>(baseURL + 'auditHistorys').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getAuditHistory(id: number): Observable<AuditHistory> {
    return this.http.get<AuditHistory>(baseURL + 'auditHistory/' + id)
      .map(res => {
        return res;
      });
  }

  getFeaturedAuditHistory(): Observable<AuditHistory> {
    return this.http.get<AuditHistory>(baseURL + 'auditHistory?featured=true')
      .map(res => {
        return res[0];
      }).catch(error => {
        console.log('error: ' + error);
        return error;
      });
  }

  addAuditHistory(auditHistory: AuditHistory): void {
    this.http.post(baseURL + 'auditHistory', auditHistory).subscribe(data => {
        this.dialogData = auditHistory;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateAuditHistory(auditHistory: AuditHistory): void {
    this.http.put(baseURL + 'auditHistory', auditHistory).subscribe(data => {
        this.dialogData = auditHistory;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  deleteAuditHistory(id: number): void {
    this.http.delete(baseURL + 'auditHistory/' + id).subscribe(data => {
        console.log('Successfully deleted');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
}
