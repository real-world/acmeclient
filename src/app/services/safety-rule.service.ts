import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {SafetyRule} from '../shared/safetyRule';
import {baseURL} from '../shared/baseUrl';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class SafetyRuleService {
  dataChange: BehaviorSubject<SafetyRule[]> = new BehaviorSubject<SafetyRule[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private http: HttpClient) { }
  get data(): SafetyRule[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }

  getAllSafetyRules(): void {
    this.http.get<SafetyRule[]>(baseURL + 'safetyRules').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getSafetyRule(id: number): Observable<SafetyRule> {
    return this.http.get<SafetyRule>(baseURL + 'safetyRules/' + id)
      .map(res => {
        return res;
      });
  }
  addSafetyRule(safetyRule: SafetyRule): void {
    this.http.post(baseURL + 'safetyRules', safetyRule).subscribe(data => {
        this.dialogData = safetyRule;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateSafetyRule(safetyRule: SafetyRule): void {
    this.http.put(baseURL + 'safetyRules', safetyRule).subscribe(data => {
        this.dialogData = safetyRule;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
  deleteSafetyRule(id: number): void {
    this.http.delete(baseURL + 'safetyRules/' + id).catch(error => {
      console.log('error' + error);
      return error;
    });
  }
}
