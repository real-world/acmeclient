import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Incident } from '../shared/incident';
import { IncidentRegistry } from '../shared/incidentRegistry';
import { INCIDENTS } from '../shared/incidents';
import { BehaviorSubject, Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class IncidentService {

  dataChange: BehaviorSubject<Incident[]> = new BehaviorSubject<Incident[]>([]);

  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): Incident[] {
    return this.dataChange.value;
  }

  getIncidents(): Incident[] {
    return INCIDENTS;
  }

  getIncident(id: number): Incident {
    return INCIDENTS.filter((incident) => incident.id === id)[0];
  }

  getFeaturedIncident(): Incident {
    return INCIDENTS.filter((incident) => incident.title)[0];
  }
}
