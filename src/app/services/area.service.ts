import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Area} from '../shared/area';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {baseURL} from '../shared/baseUrl';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class AreaService {
  dataChange: BehaviorSubject<Area[]> = new BehaviorSubject<Area[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private http: HttpClient) { }
  get data(): Area[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }

  getAllAreas(): void {
    this.http.get<Area[]>(baseURL + 'areas').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getAllAreasToProject() {
    return this.http.get<Area[]>(baseURL + 'areas');
    // this.http.get<Area[]>(baseURL + 'areas').subscribe(data => {
    //     this.dataRead = data;
    //     console.log();
    //   },
    //   (error: HttpErrorResponse) => {
    //     console.log(error.name + ' ' + error.message);
    //   });
  }

  getArea(id: number): Observable<Area> {
    return this.http.get<Area>(baseURL + 'areas/' + id)
      .map(res => {
        return res;
      });
  }
  addArea(area: Area): void {
    this.http.post(baseURL + 'areas', area).subscribe(data => {
        this.dialogData = area;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateArea(area: Area): void {
    this.http.put(baseURL + 'areas', area).subscribe(data => {
        this.dialogData = area;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
  deleteArea(id: number): void {
    this.http.delete(baseURL + 'areas/' + id).subscribe(data => {
      console.log('Successfully deleted');
    });
  }
}
