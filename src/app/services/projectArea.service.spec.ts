import {inject, TestBed} from '@angular/core/testing';

import {ProjectAreaService} from './projectArea.service';

describe('ProjectAreaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectAreaService]
    });
  });

  it('should be created', inject([ProjectAreaService], (service: ProjectAreaService) => {
    expect(service).toBeTruthy();
  }));
});
