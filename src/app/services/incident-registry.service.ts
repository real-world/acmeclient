import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {baseURL} from '../shared/baseUrl';
import {IncidentRegistry} from '../shared/incidentRegistry';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';


@Injectable()
export class IncidentRegistryService {

  dataChange: BehaviorSubject<IncidentRegistry[]> = new BehaviorSubject<IncidentRegistry[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): IncidentRegistry[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllIncidents(): void {
    this.http.get<IncidentRegistry[]>(baseURL + 'incidents').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getIncident(id: number): Observable<IncidentRegistry> {
    return this.http.get<IncidentRegistry>(baseURL + 'incidents/' + id)
      .map(res => {
        return res;
      });
  }

  addIncidentRegistry(incidentRegistry: IncidentRegistry): void {
    this.http.post(baseURL + 'incidents', incidentRegistry).subscribe(data => {
        this.dialogData = incidentRegistry;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateIncidentRegistry(incidentRegistry: IncidentRegistry): void {
    this.http.put(baseURL + 'incidents', incidentRegistry).subscribe(data => {
        this.dialogData = incidentRegistry;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  deleteEmployee(id: number): void {
    this.http.delete(baseURL + 'incidents/' + id).subscribe(data => {
        console.log('Successfully deleted');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
}

