import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Category} from '../shared/category';
import {baseURL} from '../shared/baseUrl';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';


@Injectable()
export class CategoryService {

  dataChange: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>([]);
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Category[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllCategories(): void {
    this.httpClient.get<Category[]>(baseURL + 'categories').subscribe(data => {
      console.log("CATEGORIESSSSSSSSSS")
      console.log(this.data)
      console.log("CATEGORIESSSSSSSSSS"+this.data)
        this.dataChange.next(data);
        
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
  }

  addCategory (category: Category): void {
    this.httpClient.post(baseURL + 'categories', category).subscribe(data => {
        this.dialogData = category;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateCategory (category: Category): void {
    this.httpClient.put(baseURL + 'categories', category).subscribe(data => {
        this.dialogData = category;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  deleteCategory(id: number): void {
    console.log("ESTOY BORRANDO ESTE ID> "+id)
    console.log(id)
    this.httpClient.delete(baseURL + 'categories/' + id).subscribe(data => {
      console.log('Successfully deleted');
    });
  }
}



