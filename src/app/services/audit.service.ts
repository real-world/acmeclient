import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {baseURL} from '../shared/baseUrl';
import {Audit} from '../shared/audit';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';


@Injectable()
export class AuditService {
  dataChange: BehaviorSubject<Audit[]> = new BehaviorSubject<Audit[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): Audit[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAudits(): void {
    this.http.get<Audit[]>(baseURL + 'audits').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getAudit(id: number): Observable<Audit> {
    return this.http.get<Audit>(baseURL + 'audits/' + id)
      .map(res => {
        return res;
      });
  }

  getFeaturedAudit(): Observable<Audit> {
    return this.http.get<Audit>(baseURL + 'audits?featured=true')
      .map(res => {
        return res[0];
      }).catch(error => {
        console.log('error: ' + error);
        return error;
      });
  }

  addAudit(audit: Audit): void {
    this.http.post(baseURL + 'audits', audit).subscribe(data => {
        this.dialogData = audit;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateAudit(audit: Audit): void {
    this.http.put(baseURL + 'audits', audit).subscribe(data => {
        this.dialogData = audit;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  deleteAudit(id: number): void {
    this.http.delete(baseURL + 'audits/' + id).subscribe(data => {
        console.log('Successfully deleted');
        window.location.reload();
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
}
