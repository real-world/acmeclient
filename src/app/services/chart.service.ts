import { Injectable } from '@angular/core';
import {DateReportModel} from '../shared/dateReportModel';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Eventuality} from '../shared/eventuality';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {baseURL} from '../shared/baseUrl';

@Injectable()
export class ChartService {

  dataChange: BehaviorSubject<DateReportModel[]> = new BehaviorSubject<DateReportModel[]>([]);
  dialogData: any;
  public chartData;
  loadReportbyDate(startDate: string, endDate: string, employeeId: number,
    employeeName: string, typeEventId: number){
    //return this.http.get(baseURL + 'reportGraph/'+employeeName +'/'++'/0/'null'/'null');
    //return this.http.get(baseURL + 'reportGraph/M/null/0/2018-01-13/2018-12-13');
    console.log(startDate);
    console.log(endDate);
    console.log(employeeName);
    console.log(typeEventId);
    return this.http.get(baseURL + 'reportGraph/' + employeeName + '/' + typeEventId + '/0/' + startDate + '/' + endDate);
  }

  public loadDatas(){
    this.http.get(baseURL + 'reportGraph/null/null/0/null/null').subscribe(data => {
      this.chartData = data;
    });
  }
  constructor(private http: HttpClient) { }
}
