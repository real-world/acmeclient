import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {baseURL} from '../shared/baseUrl';
import {BehaviorSubject, Observable} from 'rxjs';
import {Training} from '../shared/training';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class TrainingService {
  dataChange: BehaviorSubject<Training[]> = new BehaviorSubject<Training[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private http: HttpClient) { }
  get data(): Training[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }

  getAllTraininies(): void {
    this.http.get<Training[]>(baseURL + 'traininies').subscribe(data => {
        this.dataChange.next(data);
        console.log(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getTraining(id: number): Observable<Training> {
    return this.http.get<Training>(baseURL + 'traininies/' + id)
      .map(res => {
        return res;
      });
  }
  addTraining(training: Training): void {
    this.http.post(baseURL + 'traininies', training).subscribe(data => {
        this.dialogData = training;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateTraining(training: Training): void {
    this.http.put(baseURL + 'traininies', training).subscribe(data => {
        this.dialogData = training;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
  deleteTraining(id: number): void {
    this.http.delete(baseURL + 'traininies/' + id).subscribe(data => {
      console.log('Successfully deleted');
    });
  }
}

