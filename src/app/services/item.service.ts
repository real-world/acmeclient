import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Item} from '../shared/item';
import {baseURL} from '../shared/baseUrl';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class ItemService {

  dataChange: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>([]);
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Item[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllItems(): void {
    console.log('test');
    this.httpClient.get<Item[]>(baseURL + 'items').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
  }
  getItem(id: number): Observable<Item> {
    return this.httpClient.get<Item>(baseURL + 'items/' + id)
      .map(res => {
        return res;
      });
  }

  addItem (item: Item): void {
    this.httpClient.post(baseURL + 'items', item).subscribe(data => {
        this.dialogData = item;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateItem (item: Item): void {
    this.httpClient.put(baseURL + 'items', item).subscribe(data => {
        this.dialogData = item;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  deleteItem(id: number): void {
    this.httpClient.delete(baseURL + 'items/' + id).subscribe(data => {
        console.log('Successfully deleted');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
}

