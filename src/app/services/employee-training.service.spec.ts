import { TestBed, inject } from '@angular/core/testing';

import { EmployeeTrainingService } from './employee-training.service';

describe('EmployeeTrainingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeTrainingService]
    });
  });

  it('should be created', inject([EmployeeTrainingService], (service: EmployeeTrainingService) => {
    expect(service).toBeTruthy();
  }));
});
