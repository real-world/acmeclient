import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {baseURL} from '../shared/baseUrl';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Position} from '../shared/position';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class PositionService {
  dataChange: BehaviorSubject<Position[]> = new BehaviorSubject<Position[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private http: HttpClient) { }
  get data(): Position[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }

  getAllPositions(): void {
    this.http.get<Position[]>(baseURL + 'positions').subscribe(data => {
        this.dataChange.next(data);
        console.log(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getPosition(id: number): Observable<Position> {
    return this.http.get<Position>(baseURL + 'positions/' + id)
      .map(res => {
        return res;
      });
  }
  addPosition(position: Position): void {
    this.http.post(baseURL + 'positions', position).subscribe(data => {
        this.dialogData = position;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updatePosition(position: Position): void {
    this.http.put(baseURL + 'positions', position).subscribe(data => {
        this.dialogData = position;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
  deletePosition(id: number): void {
    this.http.delete(baseURL + 'positions/' + id).subscribe(data => {
      console.log('Successfully deleted');
    });
  }
}
