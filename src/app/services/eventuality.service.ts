import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {baseURL} from '../shared/baseUrl';
import {Eventuality} from '../shared/eventuality';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class EventualityService {
  dataChange: BehaviorSubject<Eventuality[]> = new BehaviorSubject<Eventuality[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): Eventuality[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllEventualities(): void {
    this.http.get<Eventuality[]>(baseURL + 'eventualities').subscribe(data => {
        this.dataChange.next(data);
        console.log(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getEventuality(id: number): Observable<Eventuality> {
    return this.http.get<Eventuality>(baseURL + 'eventualities/' + id)
      .map(res => {
        return res;
      });
  }

  /*  addEventuality(id: number, eventuality: Eventuality): void {
      this.http.post(baseURL + 'eventualities/' + id + '/add', eventuality).subscribe(data => {
          this.dialogData = eventuality;
          console.log('Successfully added');
        },
        (err: HttpErrorResponse) => {
          console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
        });
    }*/
  addEventuality(eventuality: Eventuality): void {
    this.http.post(baseURL + 'eventualities', eventuality).subscribe(data => {
        this.dialogData = eventuality;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
  updateEventuality(eventuality: Eventuality): void {
    this.http.put(baseURL + 'eventualities', eventuality).subscribe(data => {
        this.dialogData = eventuality;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  deleteEventuality(id: number): void {
    this.http.delete(baseURL + 'eventualities/' + id).subscribe(data => {
        console.log('Successfully deleted');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
}
