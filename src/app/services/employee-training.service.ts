import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import {BehaviorSubject, Observable} from 'rxjs';
import {baseURL} from '../shared/baseUrl';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {EmployeeTraining} from '../shared/employeeTraining';

@Injectable()
export class EmployeeTrainingService {dataChange: BehaviorSubject<EmployeeTraining[]> = new BehaviorSubject<EmployeeTraining[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): EmployeeTraining[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllEmployeeTraininies(): void {
    this.http.get<EmployeeTraining[]>(baseURL + 'employeeTraining').subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getEmployee(id: number): Observable<EmployeeTraining> {
    return this.http.get<EmployeeTraining>(baseURL + 'employeeTraining/' + id)
      .map(res => {
        return res;
      });
  }

  getFeaturedEmployee(): Observable<EmployeeTraining> {
    return this.http.get<EmployeeTraining>(baseURL + 'employeeTraining?featured=true')
      .map(res => {
        return res[0];
      }).catch(error => {
        console.log('error: ' + error);
        return error;
      });
  }

  addEmployeeTraining(employeeTraining: EmployeeTraining): void {
    this.http.post(baseURL + 'employeeTraining', employeeTraining).subscribe(data => {
        this.dialogData = employeeTraining;
        console.log('Successfully added');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateEmployeeTraining(employeeTraining: EmployeeTraining): void {
    this.http.put(baseURL + 'employeeTraining', employeeTraining).subscribe(data => {
        this.dialogData = employeeTraining;
        console.log('Successfully updated');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  deleteEmployeeTraining(id: number): void {
    this.http.delete(baseURL + 'employeeTraining/' + id).subscribe(data => {
        console.log('Successfully deleted');
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }
}
