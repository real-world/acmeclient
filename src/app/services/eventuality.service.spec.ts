import { TestBed, inject } from '@angular/core/testing';

import { EventualityService } from './eventuality.service';

describe('EventualityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventualityService]
    });
  });

  it('should be created', inject([EventualityService], (service: EventualityService) => {
    expect(service).toBeTruthy();
  }));
});
