import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SubcategoryService} from '../services/subcategory.service';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {DataSource} from '@angular/cdk/table';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {SubCategoryAddDialogComponent} from './dialogs/add/add.dialog.component';
import {SubCategoryEditDialogComponent} from './dialogs/edit/edit.dialog.component';
import {SubCategoryDeleteDialogComponent} from './dialogs/delete/delete.dialog.component';
import {Subcategory} from '../shared/subcategory';


@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss'],
  providers: [SubcategoryService]
})
export class SubcategoryComponent implements OnInit {

  displayedColumns = ['id', 'code', 'name', 'description', 'actions'];
  subcategoryDatabase: SubcategoryService | null;
  dataSource: SubCategoryDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public dataService: SubcategoryService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  addNew(subcategory: Subcategory) {
    const dialogRef = this.dialog.open(SubCategoryAddDialogComponent, {
      data: {subcategory: subcategory}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.subcategoryDatabase.dataChange.value.push(this.dataService.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }

  startEdit(i: number, id: number, code: string, name: string, description: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(SubCategoryEditDialogComponent, {
      data: {id: id, code: code, name: name, description: description}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.subcategoryDatabase.dataChange.value.findIndex(x => x.id === this.id);
        // Then you update that record using data from dialogData (values you enetered)
        this.subcategoryDatabase.dataChange.value[foundIndex] = this.dataService.getDialogData();
        // And lastly refresh table
        this.loadData();
        this.refreshTable();
      }
    });
  }

  deleteItem(i: number, id: number, code: string, name: string, description: string, createdOn: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(SubCategoryDeleteDialogComponent, {
      data: {id: id, code: code, name: name, description: description}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.subcategoryDatabase.dataChange.value.findIndex(x => x.id === this.id);
        // for delete we use splice in order to remove single object from DataService
        this.subcategoryDatabase.dataChange.value.splice(foundIndex, 1);
        this.loadData();
        this.refreshTable();
      }
    });
  }


  // If you don't need a filter or a pagination this can be simplified, you just use code from else block
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

  public loadData() {
    this.subcategoryDatabase = new SubcategoryService(this.httpClient);
    this.dataSource = new SubCategoryDataSource(this.subcategoryDatabase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}


export class SubCategoryDataSource extends DataSource<Subcategory> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Subcategory[] = [];
  renderedData: Subcategory[] = [];

  constructor(public _subcategoryDatabase: SubcategoryService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Subcategory[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._subcategoryDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._subcategoryDatabase.getAllSubCategories();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._subcategoryDatabase.data.slice().filter((subcategory: Subcategory) => {
        const searchStr = (subcategory.id + subcategory.code + subcategory.name + subcategory.description).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }


  /** Returns a sorted copy of the database data. */
  sortData(data: Subcategory[]): Subcategory[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'code':
          [propertyA, propertyB] = [a.code, b.code];
          break;
        case 'name':
          [propertyA, propertyB] = [a.name, b.name];
          break;
        case 'description':
          [propertyA, propertyB] = [a.description, b.description];
          break;
        case 'createdOn':
          [propertyA, propertyB] = [a.createdOn, b.createdOn];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
