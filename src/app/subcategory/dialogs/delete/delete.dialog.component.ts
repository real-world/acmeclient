import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {SubcategoryService} from '../../../services/subcategory.service';


@Component({
  selector: 'app-delete.dialog',
  templateUrl: './delete.dialog.html',
  styleUrls: ['./delete.dialog.scss']
})
export class SubCategoryDeleteDialogComponent {

  constructor(public dialogRef: MatDialogRef<SubCategoryDeleteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: SubcategoryService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteSubCategory(this.data.id);
  }
}
