import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Subcategory} from '../../../shared/subcategory';
import {SubcategoryService} from '../../../services/subcategory.service';

@Component({
  selector: 'app-add.dialog',
  templateUrl: './add.dialog.html',
  styleUrls: ['./add.dialog.scss']
})

export class SubCategoryAddDialogComponent {
  constructor(public dialogRef: MatDialogRef<SubCategoryAddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Subcategory,
              public dataService: SubcategoryService) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addSubCategory(this.data);
  }
}
