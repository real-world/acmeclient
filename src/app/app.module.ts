import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {baseURL} from './shared/baseUrl';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import 'hammerjs';


import {PositionComponent} from './position/position.component';
import {PositionService} from './services/position.service';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {AboutComponent} from './about/about.component';
import {HomeComponent} from './home/home.component';
import {ContactComponent} from './contact/contact.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {LoginComponent} from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {CatalogComponent} from './catalog/catalog.component';
import {TrainingComponent} from './training/training.component';
import {EmployeeComponent} from './employee/employee.component';
import {AddComponent} from './employee/dialog/add/add.component';
import {EditComponent} from './employee/dialog/edit/edit.component';
import {DeleteComponent} from './employee/dialog/delete/delete.component';
import {EmployeeService} from './services/employee.service';
import {IncidentComponent} from './incident/incident.component';
import {IncidentDetailComponent} from './incident-detail/incident-detail.component';
import {IncidentService} from './services/incident.service';
import {ItemComponent} from './item/item.component';
import {CategoryComponent} from './category/category.component';
import {SubcategoryComponent} from './subcategory/subcategory.component';
import {ItemService} from './services/item.service';
import {CategoryService} from './services/category.service';
import {SubcategoryService} from './services/subcategory.service';
import {SkillComponent} from './skill/skill.component';
import {RoleComponent} from './role/role.component';
import {AreaComponent} from './area/area.component';
import {AddAreaComponent} from './area/dialog/add-area/add-area.component';
import {TrainingService} from './services/training.service';
import {SkillService} from './services/skill.service';
import {RoleService} from './services/role.service';
import {AreaService} from './services/area.service';
import {EventualityService} from './services/eventuality.service';
import {EditAreaComponent} from './area/dialog/edit-area/edit-area.component';
import {DeleteAreaComponent} from './area/dialog/delete-area/delete-area.component';
import {DeletePositionComponent} from './position/dialog/delete-position/delete-position.component';
import {AddPositionComponent} from './position/dialog/add-position/add-position.component';
import {EditPositionComponent} from './position/dialog/edit-position/edit-position.component';
import {EditRoleComponent} from './role/dialog/edit-role/edit-role.component';
import {AddRoleComponent} from './role/dialog/add-role/add-role.component';
import {DeleteRoleComponent} from './role/dialog/delete-role/delete-role.component';
import {DeleteSkillComponent} from './skill/dialog/delete-skill/delete-skill.component';
import {AddSkillComponent} from './skill/dialog/add-skill/add-skill.component';
import {EditSkillComponent} from './skill/dialog/edit-skill/edit-skill.component';
import {AddTrainingComponent} from './training/dialog/add-training/add-training.component';
import {EditTrainingComponent} from './training/dialog/edit-training/edit-training.component';
import {DeleteTrainingComponent} from './training/dialog/delete-training/delete-training.component';
import {AccidentComponent} from './accident/accident.component';
import {AddItemComponent} from './employee/dialog/add-item/add-item.component';
import {AuditoryComponent} from './auditory/auditory.component';
import {ProjectComponent} from './project/project.component';
import {ProjectService} from './services/project.service';
import {ProjectAreaService} from './services/projectArea.service';
import {CategoryAddDialogComponent} from './category/dialogs/add/add.dialog.component';
import {CategoryDeleteDialogComponent} from './category/dialogs/delete/delete.dialog.component';
import {CategoryEditDialogComponent} from './category/dialogs/edit/edit.dialog.component';
import {ItemAddDialogComponent} from './item/dialogs/add/add.dialog.component';
import {ItemEditDialogComponent} from './item/dialogs/edit/edit.dialog.component';
import {ItemDeleteDialogComponent} from './item/dialogs/delete/delete.dialog.component';
import {IncidentRegistryService} from './services/incident-registry.service';
import {ContractModule} from './contract/contract.module';
import {EventualityComponent} from './eventuality/eventuality.component';
import {UpdateComponent} from './eventuality/dialog/update/update.component';
import {EvenualityDeleteComponent} from './eventuality/dialog/delete/delete.component';
import {EventualityAddComponent} from './eventuality/dialog/add/add.component';
import {EmployeeTrainingComponent} from './employee-training/employee-training.component';
import {EmployeeTrainingService} from './services/employee-training.service';
import {SubCategoryAddDialogComponent} from './subcategory/dialogs/add/add.dialog.component';
import {SubCategoryDeleteDialogComponent} from './subcategory/dialogs/delete/delete.dialog.component';
import {SubCategoryEditDialogComponent} from './subcategory/dialogs/edit/edit.dialog.component';

import {AddAreaToProjectComponent} from './project/AddProject/addAreaToProject/addAreaToProject.component';
import {AddProjectComponent} from './project/AddProject/add/add.component';
import {EditProjectComponent} from './project/AddProject/edit/edit.component';
import {DeleteProjectComponent} from './project/AddProject/delete/delete.component';
import {AuditComponent} from './audit/audit.component';
import {AuditService} from './services/audit.service';
import {SafetyRuleComponent} from './safety-rule/safety-rule.component';
import {SafetyRuleService} from './services/safety-rule.service';
import {AddAuditComponent} from './audit/dialog/add-audit/add-audit.component';
import {EditAuditComponent} from './audit/dialog/edit-audit/edit-audit.component';
import {DeleteAuditComponent} from './audit/dialog/delete-audit/delete-audit.component';
import {AddSafetyRuleComponent} from './safety-rule/dialog/add-safety-rule/add-safety-rule.component';
import {EditSafetyRuleComponent} from './safety-rule/dialog/edit-safety-rule/edit-safety-rule.component';
import {DeleteSafetyRuleComponent} from './safety-rule/dialog/delete-safety-rule/delete-safety-rule.component';

import {GraphicModule} from './graphic/graphic.module';
import {AuditHistoryComponent} from './auditHistory/auditHistory.component';
import {AuditHistoryService} from './services/auditHistory.service';
import { ChartComponent } from './chart/chart.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import {ChartService} from './services/chart.service';
// import { AccidentEmployeeComponent } from './report/accident-employee/accident-employee.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PositionComponent,
    AddPositionComponent,
    DeletePositionComponent,
    EditPositionComponent,
    FooterComponent,
    AboutComponent,
    HomeComponent,
    ContactComponent,
    LoginComponent,
    CatalogComponent,
    TrainingComponent,
    AddTrainingComponent,
    DeleteTrainingComponent,
    EditTrainingComponent,
    EmployeeComponent,
    AddComponent,
    EditComponent,
    DeleteComponent,
    AreaComponent,
    AddAreaComponent,
    DeleteAreaComponent,
    EditAreaComponent,
    RoleComponent,
    AddRoleComponent,
    DeleteRoleComponent,
    EditRoleComponent,
    SkillComponent,
    AddSkillComponent,
    EditSkillComponent,
    DeleteSkillComponent,
    DeleteComponent,
    IncidentComponent,
    IncidentDetailComponent,
    ItemComponent,
    CategoryComponent,
    SubcategoryComponent,
    AccidentComponent,
    AddItemComponent,
    AddAreaComponent,
    AddAreaToProjectComponent,
    AuditoryComponent,
    ProjectComponent,
    CategoryAddDialogComponent,
    CategoryDeleteDialogComponent,
    CategoryEditDialogComponent,
    ItemAddDialogComponent,
    ItemEditDialogComponent,
    ItemDeleteDialogComponent,
    EventualityComponent,
    UpdateComponent,
    EvenualityDeleteComponent,
    EventualityAddComponent,
    EmployeeTrainingComponent,
    DeleteProjectComponent,
    EditProjectComponent,
    AddProjectComponent,
    SubCategoryAddDialogComponent,
    SubCategoryDeleteDialogComponent,
    SubCategoryEditDialogComponent,
    AuditComponent,
    AddAuditComponent,
    DeleteAuditComponent,
    EditAuditComponent,
    SafetyRuleComponent,
    AddSafetyRuleComponent,
    DeleteSafetyRuleComponent,
    EditSafetyRuleComponent,
    // AccidentEmployeeComponent,
    AuditHistoryComponent,
    ChartComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule,
    MatDialogModule, MatExpansionModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule,
    MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule,
    MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatTableModule,
    MatTabsModule, MatToolbarModule, MatTooltipModule,
    FlexLayoutModule, FlexLayoutModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    ContractModule,
    GraphicModule,
    ChartsModule,
  ],
  providers: [
    PositionService,
    EmployeeService,
    AreaService,
    RoleService,
    SkillService,
    TrainingService,
    EmployeeTrainingService,
    IncidentService,
    CategoryService,
    ItemService,
    SubcategoryService,
    ProjectService,
    ProjectAreaService,
    IncidentRegistryService,
    EventualityService,
    AuditService,
    SafetyRuleService,
    AuditHistoryService,
    ChartService,
    {provide: 'BaseURL', useValue: baseURL}
  ],
  entryComponents: [
    LoginComponent,
    AddComponent,
    EditComponent,
    DeleteComponent,
    AddAreaComponent,
    EditAreaComponent,
    DeleteAreaComponent,
    AddRoleComponent,
    DeleteRoleComponent,
    EditRoleComponent,
    AddSkillComponent,
    EditSkillComponent,
    DeleteSkillComponent,
    AddPositionComponent,
    EditPositionComponent,
    DeletePositionComponent,
    AddTrainingComponent,
    EditTrainingComponent,
    DeleteTrainingComponent,
    AddItemComponent,
    UpdateComponent,
    EvenualityDeleteComponent,
    EventualityAddComponent,
    DeleteProjectComponent,
    EditProjectComponent,
    ItemAddDialogComponent,
    ItemEditDialogComponent,
    ItemDeleteDialogComponent,
    AddProjectComponent,
    CategoryAddDialogComponent,
    CategoryDeleteDialogComponent,
    CategoryEditDialogComponent,
    SubCategoryAddDialogComponent,
    SubCategoryDeleteDialogComponent,
    SubCategoryEditDialogComponent,
    AddAuditComponent,
    EditAuditComponent,
    DeleteAuditComponent,
    AddSafetyRuleComponent,
    EditSafetyRuleComponent,
    DeleteSafetyRuleComponent,
    AddAreaToProjectComponent,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
