import {Routes} from '@angular/router';

import {HomeComponent} from '../home/home.component';
import {RoleComponent} from '../role/role.component';
import {AreaComponent} from '../area/area.component';
import {TrainingComponent} from '../training/training.component';
import {PositionComponent} from '../position/position.component';
import {ContactComponent} from '../contact/contact.component';
import {AboutComponent} from '../about/about.component';
import {EmployeeComponent} from '../employee/employee.component';
import {IncidentComponent} from '../incident/incident.component';
import {AccidentComponent} from '../accident/accident.component';
import {AuditoryComponent} from '../auditory/auditory.component';
import {ProjectComponent} from '../project/project.component';
import {IncidentDetailComponent} from '../incident-detail/incident-detail.component';
import {EmployeeTrainingComponent} from '../employee-training/employee-training.component';
import {ItemComponent} from '../item/item.component';
import {CategoryComponent} from '../category/category.component';
import {SubcategoryComponent} from '../subcategory/subcategory.component';
import {EventualityComponent} from '../eventuality/eventuality.component';
import {AuditComponent} from '../audit/audit.component';
import {SafetyRuleComponent} from '../safety-rule/safety-rule.component';
import {AuditHistoryComponent} from '../auditHistory/auditHistory.component';
import {ChartComponent} from '../chart/chart.component';

export const routes: Routes = [
  { path: 'home',  component: HomeComponent },
  {path: 'area', component: AreaComponent},
  {path: 'role', component: RoleComponent},
  // {path: 'skill', component: SkillComponent},
  {path: 'position', component: PositionComponent},
  {path: 'training', component: TrainingComponent},
  {path: 'employee', component: EmployeeComponent},
  { path: 'contact',     component: ContactComponent },
  { path: 'about',     component: AboutComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'incident', component: IncidentComponent},
  {path: 'accident', component: AccidentComponent},
  {path: 'auditory', component: AuditoryComponent},
  {path: 'project', component: ProjectComponent},
  {path: 'incidentdetail/:id', component: IncidentDetailComponent},
  {path: 'accident', component: AccidentComponent},
  {path: 'EmployeeTraining', component: EmployeeTrainingComponent},
  {path: 'accident', component: AccidentComponent},
  {path: 'item', component: ItemComponent},
  {path: 'category', component: CategoryComponent},
  {path: 'subcategory', component: SubcategoryComponent},
  {path: 'eventualities', component: EventualityComponent},
  {path: 'audit', component: AuditComponent},
  {path: 'safetyRule', component: SafetyRuleComponent},
  {path: 'auditHistory', component: AuditHistoryComponent},
  {path: 'chart', component: ChartComponent},
];
