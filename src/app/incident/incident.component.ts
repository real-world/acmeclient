import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {Incident} from '../shared/incident';
import {IncidentRegistry} from '../shared/incidentRegistry';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {BehaviorSubject, Observable} from 'rxjs';
import {DataSource} from '@angular/cdk/table';
import {EditComponent} from '../employee/dialog/edit/edit.component';
import {AddComponent} from '../employee/dialog/add/add.component';
import {DeleteComponent} from '../employee/dialog/delete/delete.component';
import {AddItemComponent} from '../employee/dialog/add-item/add-item.component';
import { IncidentRegistryService } from '../services/incident-registry.service';
import { IncidentService } from '../services/incident.service';

@Component({
  selector: 'app-incident',
  templateUrl: './incident.component.html',
  styleUrls: ['./incident.component.scss'],
  providers: [IncidentRegistryService],
})
export class IncidentComponent implements OnInit {

  displayedColumns = ['id', 'title', 'description', 'version'];
  dataBase: IncidentRegistryService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;

  incidents: IncidentRegistry[];

  selectedIncident: IncidentRegistry;

  incidentRegistryForm: FormGroup;
  incidentRegistry: IncidentRegistry;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public incidentRegistryService: IncidentRegistryService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  addNew(incidentRegistry: IncidentRegistry) {
    const dialogRef = this.dialog.open(AddComponent, {
      data: {incidentRegistry: incidentRegistry}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.dataBase.dataChange.value.push(this.incidentRegistryService.getDialogData());
        this.loadData();
      }
    });
  }

  startEdit(i: number, id: number, title: string, description: string, version: number) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(EditComponent, {
      data: {
        id: id, title: title, description: description, version: version
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        // Then you update that record using data from dialogData (values you enetered)
        this.dataBase.dataChange.value[foundIndex] = this.incidentRegistryService.getDialogData();
        // And lastly refresh table
        this.loadData();
      }
    });
  }
  addItem() {

    // this.index = i;
    console.log('adding item');
    const dialogRef = this.dialog.open(AddItemComponent, {
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        // Then you update that record using data from dialogData (values you enetered)
        this.dataBase.dataChange.value[foundIndex] = this.incidentRegistryService.getDialogData();
        // And lastly refresh table
        this.loadData();
      }
    });
  }

  public loadData() {
    this.dataBase = new IncidentRegistryService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}


export class ExampleDataSource extends DataSource<IncidentRegistry> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: IncidentRegistry[] = [];
  renderedData: IncidentRegistry[] = [];

  constructor(public _incidentData: IncidentRegistryService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<IncidentRegistry[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._incidentData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    console.log('beforeeeee');
    this._incidentData.getAllIncidents();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._incidentData.data.slice().filter((incident: IncidentRegistry) => {
        if (incident !== undefined) {
          const searchStr = (incident.id + incident.title + incident.description + incident.version ).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }


  /** Returns a sorted copy of the database data. */
  sortData(data: IncidentRegistry[]): IncidentRegistry[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'title':
          [propertyA, propertyB] = [a.title, b.title];
          break;
        case 'description':
          [propertyA, propertyB] = [a.description, b.description];
          break;
        case 'version':
          [propertyA, propertyB] = [a.version, b.version];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
