import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {CategoryService} from '../../../services/category.service';
import {FormControl, Validators} from '@angular/forms';
import {Category} from '../../../shared/category';

@Component({
  selector: 'app-add.dialog',
  templateUrl: './add.dialog.html',
  styleUrls: ['./add.dialog.scss']
})

export class CategoryAddDialogComponent {
  constructor(public dialogRef: MatDialogRef<CategoryAddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Category,
              public dataService: CategoryService) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addCategory(this.data);
  }
}
