import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {CategoryService} from '../../../services/category.service';


@Component({
  selector: 'app-delete.dialog',
  templateUrl: './delete.dialog.html',
  styleUrls: ['./delete.dialog.scss']
})
export class CategoryDeleteDialogComponent {

  constructor(public dialogRef: MatDialogRef<CategoryDeleteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: CategoryService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteCategory(this.data.id);
  }
}
