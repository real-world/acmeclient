import {RegisterContractComponent} from './register/register.contract.component';
import {ContractListComponent} from './list/contract.list.component';

export {RegisterContractComponent, ContractListComponent};
