import {Component, OnInit} from '@angular/core';
import {ContractService} from '../service/contract.service';

@Component({
  selector: 'app-contract-list',
  templateUrl: './contract.list.component.html',
  styleUrls: ['./contract.list.component.scss']
})
export class ContractListComponent implements OnInit {
  contractList;

  constructor(private contractService: ContractService) {
  }

  ngOnInit() {
    this.contractService.getContractList()
      .subscribe(
        (response) => {
          this.contractList = response;
        }
      );
  }
}
