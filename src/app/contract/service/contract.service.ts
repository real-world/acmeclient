import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CONTRACT_LIST, POSITIONS_LIST, PROJECT_LIST, SAVE_CONTRACT, SEARCH_EMPLOYEE, TYPE_CONTRACT_LIST} from '../constants/end.points';
import {Observable} from 'rxjs';
import {Employee} from '../model/employee';
import {Contract} from '../model/contract';

@Injectable()
export class ContractService {
  constructor(private http: HttpClient) {

  }

  getProjectList() {
    return this.http.get(PROJECT_LIST);
  }

  getPositionList() {
    return this.http.get(POSITIONS_LIST);
  }

  getTypeContractList() {
    return this.http.get(TYPE_CONTRACT_LIST);
  }

  getEmployeesList(search: string): Observable<Employee[]> {
    return this.http.get<Employee[]>(SEARCH_EMPLOYEE + search);
  }

  saveContract(contract: Contract) {
    return this.http.post(SAVE_CONTRACT, contract);
  }

  getContractList() {
    return this.http.get(CONTRACT_LIST);
  }
}
