import {environment} from '../../../environments/environment';

const env = environment.environment;
export const PROJECT_LIST: string = env + '/project';
export const POSITIONS_LIST: string = env + '/positions/not';
export const TYPE_CONTRACT_LIST: string = env + '/typeContract';
export const SEARCH_EMPLOYEE: string = env + '/employees/search/';
export const SAVE_CONTRACT: string = env + '/contract';
export const CONTRACT_LIST: string = env + '/contract';
