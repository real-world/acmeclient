import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ContractService} from '../service/contract.service';
import {Contract} from '../model/contract';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-contract',
  templateUrl: './register.contract.component.html',
  styleUrls: ['./register.contract.component.scss']
})
export class RegisterContractComponent implements OnInit {
  form: FormControl = new FormControl('');
  code: FormControl = new FormControl('', [Validators.required]);
  amount = new FormControl('', [Validators.required]);
  endDate = new FormControl({value: ''}, [Validators.required]);
  beginDate = new FormControl({value: ''}, [Validators.required]);
  employeeForm = new FormControl('');
  typeForm = new FormControl('', [Validators.required]);
  workPositionForm = new FormControl('', [Validators.required]);
  projectForm = new FormControl('', [Validators.required]);
  hiddenEmployeeForm = new FormControl('', [Validators.required]);
  typePayment = [
    'Effective',
    'Deposit Bank'
  ];
  typePaymentForm = new FormControl('', [Validators.required]);
  minDate;
  formContract: FormGroup = new FormGroup(
    {
      code: this.code,
      typeForm: this.typeForm,
      workPositionForm: this.workPositionForm,
      projectForm: this.projectForm,
      typePayment: this.typePaymentForm,
      amount: this.amount,
      endDate: this.endDate,
      beginDate: this.beginDate,
      typePaymentForm: this.typePaymentForm,
      employeeForm: this.employeeForm,
      hiddenEmployeeForm: this.hiddenEmployeeForm
    }
  );

  projectList;
  positionList;
  typeContractList;
  employeesList;
  idEmployee;

  constructor(private contractService: ContractService,
              private router: Router) {
  }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.contractService.getProjectList()
      .subscribe(
        (response) => {
          this.projectList = response;
        }
      );
    this.contractService.getPositionList()
      .subscribe(
        (response) => {
          this.positionList = response;
        }
      );
    this.contractService.getTypeContractList()
      .subscribe(
        (response) => {
          this.typeContractList = response;
        }
      );
  }

  search(event: KeyboardEvent): void {
    if (this.isCharacter(event)) {
      if (this.employeeForm.value) {
        this.contractService.getEmployeesList(this.employeeForm.value)
          .subscribe(
            (response) => {
              this.employeesList = response;
              for (let i = 0; i < this.employeesList.length; i++) {
                this.employeesList[i].fullName = this.employeesList[i].firstName
                  + ' ' + this.employeesList[i].lastName;
              }
            }
          );
      }
    }
  }

  clear(inputForm: FormControl): void {
    inputForm.setValue('');
  }

  private isCharacter(event: KeyboardEvent): boolean {
    if ((event.keyCode >= 37 && event.keyCode <= 40) || event.keyCode === 13) {
      return false;
    }
    return true;
  }

  selectEmployee(id: number) {
    this.idEmployee = id;
    this.hiddenEmployeeForm.setValue(this.getEmployeeByID(id));
  }

  getEmployeeByID(id: number) {
    for (let i = 0; i < this.employeesList.length; i++) {
      if (id === this.employeesList[i].id) {
        return this.employeesList[i].fullName;
      }
    }
    return '';
  }

  submit() {
    const contract: Contract = {
      contractAmount: this.amount.value + ' bs.',
      contractCode: this.code.value,
      initDate: this.beginDate.value,
      endDate: this.endDate.value,
      paymentType: this.typePaymentForm.value,
      positionId: this.workPositionForm.value,
      typeContractId: this.typeForm.value,
      employeeId: this.idEmployee,
      projectId: this.projectForm.value
    };
    this.contractService.saveContract(contract)
      .subscribe(
        (response) => {
          this.router.navigate(['/contract']);
        }
      );
  }

}
