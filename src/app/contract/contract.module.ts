import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContractListComponent, RegisterContractComponent} from './index';
import {ContractRoutingModule} from './contract.routing.module';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatGridListModule,
  MatInputModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {ContractService} from './service/contract.service';

@NgModule({
  imports: [
    CommonModule,
    ContractRoutingModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatGridListModule,
    MatButtonModule,
    ReactiveFormsModule
  ],
  declarations: [RegisterContractComponent, ContractListComponent],
  providers: [ContractService]
})
export class ContractModule {
}
