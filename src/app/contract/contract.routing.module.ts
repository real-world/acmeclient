import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ContractListComponent, RegisterContractComponent} from './index';

const routes: Routes = [
  {
    path: 'register-contract',
    component: RegisterContractComponent,
  }, {
    path: 'contract',
    component: ContractListComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class ContractRoutingModule {
}
