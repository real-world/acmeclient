export interface Contract {
  contractCode: string;
  paymentType: string;
  contractAmount: string;
  initDate: string;
  endDate: string;
  typeContractId: string;
  employeeId: string;
  positionId: string;
  projectId: string;
}
