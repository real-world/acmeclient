import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DataSource} from '@angular/cdk/table';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {BehaviorSubject, Observable} from 'rxjs';
import {PositionService} from '../services/position.service';
import {Position} from '../shared/position';
import {AddPositionComponent} from './dialog/add-position/add-position.component';
import {EditPositionComponent} from './dialog/edit-position/edit-position.component';
import {DeletePositionComponent} from './dialog/delete-position/delete-position.component';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss'],
  providers: [PositionService]
})
export class PositionComponent implements OnInit {
  displayedColumns = ['id', 'name', 'role', 'actions'];
  dataBase: PositionService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public positionService: PositionService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;
  ngOnInit() {
    this.loadData();
  }
  refresh() {
    this.loadData();
  }
  addNew(position: Position) {
    const dialogRef = this.dialog.open(AddPositionComponent, {
      data: {position: position}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*After dialog is closed we're doing frontend updates
        For add we're just pushing a new row inside DataService*/
        this.dataBase.dataChange.value.push(this.positionService.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }

  startEdit(i: number, id: number, name: string, role: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(EditPositionComponent, {
      data: {
        id: id, name: name, role: role
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*When using an edit things are little different,
        firstly we find record inside DataService by id*/
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*Then you update that record using data from dialogData
        (values you enetered)*/
        this.dataBase.dataChange.value[foundIndex] = this.positionService.getDialogData();
        // And lastly refresh table
        this.loadData();
        this.refreshTable();
      }
    });
  }

  deleteItem(i: number, id: number, name: string, role: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeletePositionComponent, {
      data: {
        id: id, name: name, role: role
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*For delete we use splice in order to remove
        single object from DataService*/
        this.dataBase.dataChange.value.splice(foundIndex, 1);
        this.loadData();
        this.refreshTable();
      }
    });
  }
  /*If you don't need a filter or a pagination this can be simplified,
  you just use code from else block*/
  private refreshTable() {
    /*if there's a paginator active we're using it for refresh*/
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      /* in case we're on last page this if will tick*/
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      /*in all other cases including active filter we do it like this*/
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }
  public loadData() {
    this.dataBase = new PositionService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}
export class ExampleDataSource extends DataSource<Position> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Position[] = [];
  renderedData: Position[] = [];

  constructor(public _positionData: PositionService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /*Connect function called by the table to retrieve one
  stream containing the data to render.*/
  connect(): Observable<Position[]> {
    // Listen for any changes in the base data,
    // sorting, filtering, or pagination
    const displayDataChanges = [
      this._positionData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._positionData.getAllPositions();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._positionData.data.slice().filter((position: Position) => {
        if (position !== undefined) {
          const searchStr = (position.id + position.name + position.role).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }

  /*Returns a sorted copy of the database data.*/
  sortData(data: Position[]): Position[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'code':
          [propertyA, propertyB] = [a.name, b.name];
          break;
        case 'role':
          [propertyA, propertyB] = [a.role, b.role];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}

