import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {PositionService} from '../../../services/position.service';

@Component({
  selector: 'app-delete-position',
  templateUrl: './delete-position.component.html',
  styleUrls: ['./delete-position.component.scss']
})
export class DeletePositionComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeletePositionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public positionService: PositionService) {
  }
  onCancelClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.positionService.deletePosition(this.data.id);
  }

  ngOnInit() {
  }
}
