import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {PositionService} from '../../../services/position.service';

@Component({
  selector: 'app-edit-position',
  templateUrl: './edit-position.component.html',
  styleUrls: ['./edit-position.component.scss']
})
export class EditPositionComponent implements OnInit {
  rolesPosition = [
    {value: 1, viewValue: 'Home Electicity Manager'},
    {value: 2, viewValue: 'Home Electicity Supervisor'},
    {value: 3, viewValue: 'Operator'},
    {value: 4, viewValue: 'Electicity Technical'},
    {value: 5, viewValue: 'Home Electicity Technical'},
    {value: 6, viewValue: 'Industrial Electicity Manager'},
    {value: 7, viewValue: 'Heavy Work Manager'},
    {value: 8, viewValue: 'Fine Work Supervisor'},
    {value: 9, viewValue: 'Hidrosanitary Manager'},
    {value: 10, viewValue: 'Hidrosanitary Supervisor'},
    {value: 11, viewValue: 'Modified'}
  ];
  constructor(public dialogRef: MatDialogRef<EditPositionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public positionService: PositionService) {
  }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.positionService.updatePosition(this.data);
  }

  ngOnInit() {
  }

}
