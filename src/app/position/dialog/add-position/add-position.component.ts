import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {PositionService} from '../../../services/position.service';
import {Position} from '../../../shared/position';
import {Role} from '../../../shared/role';
import {RoleService} from '../../../services/role.service';

@Component({
  selector: 'app-add-position',
  templateUrl: './add-position.component.html',
  styleUrls: ['./add-position.component.scss']
})
export class AddPositionComponent implements OnInit {

  rolesPosition = [
    {value: 1, viewValue: 'Home Electicity Manager'},
    {value: 2, viewValue: 'Home Electicity Supervisor'},
    {value: 3, viewValue: 'Operator'},
    {value: 4, viewValue: 'Electicity Technical'},
    {value: 5, viewValue: 'Home Electicity Technical'},
    {value: 6, viewValue: 'Industrial Electicity Manager'},
    {value: 7, viewValue: 'Heavy Work Manager'},
    {value: 8, viewValue: 'Fine Work Supervisor'},
    {value: 9, viewValue: 'Hidrosanitary Manager'},
    {value: 10, viewValue: 'Hidrosanitary Supervisor'},
    {value: 11, viewValue: 'Modified'}
  ];

  constructor(public dialogRef: MatDialogRef<AddPositionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Position,
              @Inject(MAT_DIALOG_DATA) public dataRole: Role,
              public positionService: PositionService,
              public roleService: RoleService) {
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.positionService.addPosition(this.data);
  }

  ngOnInit() {
  }

}
