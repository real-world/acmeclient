import { Component, OnInit, ViewChild } from '@angular/core';
import {DateReportModel} from '../shared/dateReportModel';
import {ChartService} from '../services/chart.service';
import {EmployeeService} from '../services/employee.service';
import {HttpErrorResponse} from '@angular/common/http';
import {forEach} from '@angular/router/src/utils/collection';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit {

  @ViewChild(BaseChartDirective) public testChart: BaseChartDirective;

  displayedColumns: string[] = ['employeeName', 'accidents', 'incidents'];
  dataSource ;

  public startPickerInput: Date = null;
  public endPickerInput: Date = null;
  public employeeNameLike: string = null;
  public employeeSelected: number = 0;
  public typeEventSelected: number = null;

  //bar
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  dataExport: DateReportModel[] = [];

  public barChartData: any[] = [];

  constructor(public chartService: ChartService) {
    this.chartService.loadDatas();
    setTimeout(() => {}, 3000);
  }

  ngOnInit() {
    this.chartService.loadDatas();
    const ELEMENT_DATA: DateReportModel[] = [];
    this.startPickerInput = new Date();
    let startDate= this.startPickerInput.getFullYear()-10;
    this.startPickerInput = new Date(startDate);
    this.endPickerInput = new Date();
    const auxLabel = [];
    const auxAccidents = [];
    const auxIncidents = [];

    for (let key in this.chartService.chartData) {
      let value = this.chartService.chartData[key];
      ELEMENT_DATA.push({employeeName: value.employeeName, accidents: value.numberAccident, incidents: value.numberIncident});
      auxLabel.push(value.employeeName);
      auxAccidents.push(value.numberAccident);
      auxIncidents.push(value.numberIncident);
    }
    this.dataSource = ELEMENT_DATA;
    this.dataExport = ELEMENT_DATA;
    this.barChartLabels = auxLabel;
    this.barChartData = [
      {data: auxAccidents, label: 'Accidents'},
      {data: auxIncidents, label: 'Incidents'}
    ];
  }
  ChangeDateRange(){

    const auxLabel = [];
    const auxAccidents = [];
    const auxIncidents = [];
    let fechIni: string;
    let fechFin: string;
    if ( null != this.startPickerInput && null != this.endPickerInput  ) {
      let initDate = this.startPickerInput.getUTCFullYear() + '-' + (this.startPickerInput.getUTCMonth() + 1) + '-' + this.startPickerInput.getUTCDate();
      let endDate = this.endPickerInput.getUTCFullYear() + '-' + (this.endPickerInput.getUTCMonth() + 1) + '-' + this.endPickerInput.getUTCDate();
      fechIni = initDate.toString();
      fechFin = endDate.toString();
    } else {
      fechIni = 'null';
      fechFin = 'null';
    }
    let employeeId = this.employeeSelected;
    let employeeName = this.employeeNameLike;
    let typeEventId = this.typeEventSelected;

    this.chartService.loadReportbyDate(fechIni.toString(), fechFin.toString(), employeeId, employeeName, typeEventId).subscribe(data => {

        const response: DateReportModel[] = [];
        this.barChartLabels.forEach(element => {
          auxAccidents.push(0);
          auxIncidents.push(0);
        });
        for (let key in data) {
          let value = data[key];
          response.push({employeeName: value.employeeName, accidents: value.numberAccident, incidents: value.numberIncident});
          auxAccidents[this.barChartLabels.indexOf(value.employeeName)] = value.numberAccident;
          auxIncidents[this.barChartLabels.indexOf(value.employeeName)] = value.numberIncident;
        }

        this.dataSource = response;

        let clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = auxAccidents;
        clone[1].data = auxIncidents;
        this.barChartData = clone;

      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  ExportCSV(){
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Your title',
      useBom: true,
      noDownload: true,
      headers: ["Employee Name", "Number Accident", "Number Incident"]
    };

    new Angular5Csv(this.dataExport, "ReporteCSV", options);
  }
}
