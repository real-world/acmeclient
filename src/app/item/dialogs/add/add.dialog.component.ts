import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Item} from '../../../shared/item';
import {FormControl, Validators} from '@angular/forms';
import {ItemService} from '../../../services/item.service';

@Component({
  selector: 'app-add.dialog',
  templateUrl: './add.dialog.html',
  styleUrls: ['./add.dialog.scss']
})

export class ItemAddDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ItemAddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Item,
              public dataService: ItemService) { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addItem(this.data);
  }

  ngOnInit() {
  }
}
