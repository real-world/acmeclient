import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {ItemService} from '../../../services/item.service';


@Component({
  selector: 'app-delete.dialog',
  templateUrl: './delete.dialog.html',
  styleUrls: ['./delete.dialog.scss']
})
export class ItemDeleteDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ItemDeleteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: ItemService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteItem(this.data.id);
  }

  ngOnInit() {
  }
}
