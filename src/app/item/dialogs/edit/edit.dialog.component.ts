import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {ItemService} from '../../../services/item.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-baza.dialog',
  templateUrl: './edit.dialog.html',
  styleUrls: ['./edit.dialog.scss']
})
export class ItemEditDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ItemEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: ItemService) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  // getErrorMessage() {
  //   return this.formControl.hasError('required') ? 'Required field' :
  //     this.formControl.hasError('email') ? 'Not a valid email' :
  //       '';
  // }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.dataService.updateItem(this.data);
  }
  ngOnInit() {
  }
}
