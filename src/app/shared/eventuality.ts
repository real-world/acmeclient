export interface Eventuality {
    id: number;
    type: string;
    dateEvent: string;
    injuryType: string;
    injuryPart: string;
    description: string;
    employeeId: number;
    employee: string;
}
