export interface Subcategory {
  id: number;
  name: string;
  code: string;
  description: string;
  createdOn: string;
}
