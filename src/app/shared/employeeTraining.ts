export interface EmployeeTraining {
  id: number;
  trainingId: number;
  name: string;
  instructor: string;
  employeeId: number;
  firstName: string;
  lastName: string;
}
