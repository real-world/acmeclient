export interface Area {
  id: number;
  code: string;
  name: string;
  createdOn: string;
  updatedOn: string;
  version: number;
}
