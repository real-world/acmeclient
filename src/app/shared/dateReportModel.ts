export interface DateReportModel {
  employeeName: string;
  accidents: number;
  incidents: number;  
}
