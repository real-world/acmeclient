export interface IncidentRegistry {
  id: number;
  createdOn: string;
  updatedOn: string;
  version: number;
  title: string;
  description: string;
}
