export class Position {
  id: number;
  name: string;
  role_id: number;
  role: string;
  modifiedBy: number;
}
