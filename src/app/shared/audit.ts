export interface Audit {
  id: number;
  auditName: string;
  auditCode: string;
  auditType: string;
  auditScope: string;
  auditObjective: string;
  auditCriteria: string;
  auditPeriodicity: string;
  employeeId: number;
  departmentId: number;
  createdDate: string;
}
