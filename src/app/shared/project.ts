import {Audit} from '../shared/audit';

export interface Project {
  id: number;
  name: string;
  description: string;
  date_start: string;
  date_end: string;
  area: string;
}
