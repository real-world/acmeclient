export interface Role {
  id: number;
  code: string;
  description: string;
  createdOn: string;
  updatedOn: string;
  version: number;
}
