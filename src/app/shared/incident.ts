export interface Incident {
  id: number;
  title: string;
  description: string;
  version: number;
}
