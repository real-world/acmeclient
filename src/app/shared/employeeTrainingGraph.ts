export interface DwTrainingGraphReport {
  id: number;
  createdOn;
  updatedOn;
  version: number;
  countEnventuality: number;
  employeeName: string;
}
