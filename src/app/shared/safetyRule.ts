export interface SafetyRule {
  id: number;
  policyCode: string;
  policyName: string;
  complianceParameter: number;
  complianceMetric: number;
  accomplishment: boolean;
}
