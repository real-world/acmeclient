export interface Skill {
  id: number;
  code: string;
  description: string;
}
