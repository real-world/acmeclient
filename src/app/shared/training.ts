export interface Training {
  id: number;
  code: string;
  name: string;
  instructor: string;
  area_id: number;
  area: string;
  modifiedBy: number;
}
