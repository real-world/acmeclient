import {Project} from './project';
import {Area} from './area';

export interface ProjectArea {
  id: number;
  project: Project;
  area: Area;
  estado: string;
}
