export interface Category {
  id: number;
  code: string;
  name: string;
  description: string;
  createdOn: string;
}
