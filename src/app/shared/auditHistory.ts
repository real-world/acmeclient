export interface AuditHistory {
  id: number;
  tableName: string;
  columnName: string;
  date: string;
  oldValue: string;
  newValue: string;
  modifiedBy: string;
  typeOperation: string;

}
