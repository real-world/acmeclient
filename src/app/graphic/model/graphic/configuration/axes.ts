export class Axes {
  private gridLines: GridLines = new GridLines();
  private stacked = true;
  private offset = false;
  private weight = 0;
  maxBarThickness: number;
  barThickness: number;
  ticks: Ticks = new Ticks();

  constructor() {
  }

  get gridLines$(): GridLines {
    return this.gridLines;
  }

  get stacked$(): boolean {
    return this.stacked;
  }

  get offset$(): boolean {
    return this.offset;
  }

  get weight$(): number {
    return this.weight;
  }


  set gridLines$(value: GridLines) {
    this.gridLines = value;
  }

  set stacked$(value: boolean) {
    this.stacked = value;
  }

  set offset$(value: boolean) {
    this.offset = value;
  }

  set weight$(value: number) {
    this.weight = value;
  }

  set maxBarThickness$(value: number) {
    this.maxBarThickness = value;
  }

  set barThickness$(barThickness: number) {
    this.barThickness = barThickness;
  }

  get ticks$() {
    return this.ticks;
  }
}


class GridLines {
  private display = true;
  private color = 'rgba$(0, 0, 0, 0.1)';
  private lineWidth = 1;

  constructor() {
  }

  get display$(): boolean {
    return this.display;
  }

  get color$(): string {
    return this.color;
  }

  get lineWidth$(): number {
    return this.lineWidth;
  }


  set display$(value: boolean) {
    this.display = value;
  }

  set color$(value: string) {
    this.color = value;
  }

  set lineWidth$(value: number) {
    this.lineWidth = value;
  }
}

class Ticks {
  private fontSize = 10;
  private beginAtZero = true;
  private stepSize: number;

  constructor() {
  }

  set fontSize$(value: number) {
    this.fontSize = value;
  }

  set beginAtZero$(value: boolean) {
    this.beginAtZero = value;
  }

  set stepSize$(value: number) {
    this.stepSize = value;
  }
}
