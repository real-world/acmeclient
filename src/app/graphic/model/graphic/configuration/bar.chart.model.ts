import {Legend} from './legend';
import {Scales} from './scales';

export class BarChartModel {
  private legend: Legend = new Legend();
  private responsive: boolean;
  private maintainAspectRatio: boolean;
  private scales: Scales = new Scales();
  private scaleShowVerticalLines: false;

  constructor(responsive = true, maintainAspectRatio = true) {
    this.responsive = responsive;
    this.maintainAspectRatio = maintainAspectRatio;
  }

  set responsive$(value: boolean) {
    this.responsive = value;
  }

  set maintainAspectRatio$(value: boolean) {
    this.maintainAspectRatio = value;
  }

  set scaleShowVerticalLines$(value: any) {
    this.scaleShowVerticalLines = value;
  }

  get legend$(): Legend {
    return this.legend;
  }

  get scales$(): Scales {
    return this.scales;
  }
}

