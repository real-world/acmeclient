import {Legend} from './legend';

export class PieChartModel {
  private legend: Legend = new Legend();
  private responsive: boolean;
  private maintainAspectRatio: boolean;
  private scaleShowVerticalLines: boolean;

  constructor() {
    this.responsive = true;
    this.maintainAspectRatio = true;
  }

  get legend$() {
    return this.legend;
  }

  set responsive$(responsive: boolean) {
    this.responsive = responsive;
  }

  set maintainAspectRatio$(maintainAspectRatio: boolean) {
    this.maintainAspectRatio = maintainAspectRatio;
  }

  set scaleShowVerticalLines$(scaleShowVerticalLines: boolean) {
    this.scaleShowVerticalLines = scaleShowVerticalLines;
  }
}
