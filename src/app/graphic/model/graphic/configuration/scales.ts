import {Axes} from './axes';

export class Scales {
  private xAxes: Array<Axes>;
  private yAxes: Array<Axes>;

  constructor() {
    const x: Axes = new Axes();
    const y: Axes = new Axes();
    this.xAxes = [x];
    this.yAxes = [y];
  }

  get xAxes$(): Axes {
    return this.xAxes[0];
  }

  get yAxes$(): Axes {
    return this.yAxes[0];
  }
}
