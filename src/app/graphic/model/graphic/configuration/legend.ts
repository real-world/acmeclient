export class Legend {
  private display = true;
  private position = 'top';
  private fullWidth = true;
  private labels: Label = new Label();

  constructor() {
  }


  set display$(value: boolean) {
    this.display = value;
  }

  set position$(value: string) {
    this.position = value;
  }

  set fullWidth$(value: boolean) {
    this.fullWidth = value;
  }


  get display$(): boolean {
    return this.display;
  }

  get position$(): string {
    return this.position;
  }

  get fullWidth$(): boolean {
    return this.fullWidth;
  }

  get labels$(): Label {
    return this.labels;
  }
}

class Label {
  private boxWidth = 40;
  private fontSize = 12;
  private fontStyle = 'normal';
  private fontColor = '#666';
  private fontFamily = 'Helvetica Neue,Helvetica,Arial,sans-serif';
  private padding = 10;

  constructor() {
  }

  set boxWidth$(value: number) {
    this.boxWidth = value;
  }

  set fontSize$(value: number) {
    this.fontSize = value;
  }

  set fontStyle$(value: string) {
    this.fontStyle = value;
  }

  set fontColor$(value: string) {
    this.fontColor = value;
  }

  set fontFamily$(value: string) {
    this.fontFamily = value;
  }

  set padding$(value: number) {
    this.padding = value;
  }


  get boxWidth$(): number {
    return this.boxWidth;
  }

  get fontSize$(): number {
    return this.fontSize;
  }

  get fontStyle$(): string {
    return this.fontStyle;
  }

  get fontColor$() {
    return this.fontColor;
  }

  get fontFamily$() {
    return this.fontFamily;
  }

  get padding$(): number {
    return this.padding;
  }
}
