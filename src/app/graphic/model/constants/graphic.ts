    export enum GraphicPosition {
  TOP = 'top',
  BOTTOM = 'bottom',
  LEFT = 'left',
  RIGHT = 'right'
}

export enum GraphicSizes {
  LABEL_BOX_SIZE = 10,
  MAX_WIDTH = 50,
  FONT_SIZE_LABEL = 10,
  FONT_SIZE_TICK = 10,
  MAX_SIZE_FIRST = 4,
  MIN_SIZE_FIRST = 0,
  MAX_SIZE_SECOND = 10,
  MIN_SIZE_SECOND = 5,
  MAX_SIZE_THIRD = 100,
  MIN_SIZE_THIRD = 11,
  STEPS_SIZE = 10,
}



