import {Component} from '@angular/core';

@Component({
  selector: 'app-piechart',
  templateUrl: './pie.chart.component.html',
})
export class PieChartComponent {
  public doughnutChartLabels: string[] = ['Accident', 'Incident'];
  public doughnutChartData: number[] = [350, 450];
  public doughnutChartType = 'doughnut';

  constructor() {
  }
}
