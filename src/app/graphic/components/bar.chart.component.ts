import {Component, OnInit} from '@angular/core';
import {RepoService} from '../service/repo.service';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {EmployeeService} from '../../services/employee.service';

@Component({
  selector: 'app-barchart',
  templateUrl: './bar.chart.component.html'
})
export class BarChartComponent implements OnInit{
  dataBase: RepoService;

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [];
  public barChartType = 'bar';
  public barChartLegend = true;
  public accidentes: string[] = [];
  public incidents: string[] = [];

  public barChartData: any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Eventuality'
    },
    // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Incident'}
  ];
ngOnInit(){
  this.loadData();
}
  constructor(public httpClient: HttpClient, public repoService: RepoService) {
  }
  public loadData() {
    this.repoService.getReport().subscribe(data => {
          data.forEach(repo => {
            this.barChartLabels.push(repo.employeeName);
            this.accidentes.push(String(repo.countEnventuality))
            this.incidents.push(repo.employeeName);
          });
          console.log(data);
          console.log(this.barChartLabels);
          console.log(this.barChartLabels);
          this.barChartData = [{data: this.accidentes, label: 'Accident'
          },{data: this.incidents, label: 'Incident'}];
         },
         (error: HttpErrorResponse) => {
           console.log(error.name + ' ' + error.message);
         });
  }
}
