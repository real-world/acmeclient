import {NgModule} from '@angular/core';
import {ChartsModule} from 'ng2-charts';
import {ExampleComponent} from './example.component';
import {GraphicRoutingModule} from './graphic.routing.module';
import {BarChartComponent} from './components/bar.chart.component';
import {PieChartComponent} from './components/pie.chart.component';

@NgModule({
  imports: [
    ChartsModule,
    GraphicRoutingModule
  ],
  declarations: [
    BarChartComponent,
    PieChartComponent,
    ExampleComponent
  ]
})
export class GraphicModule {

}
