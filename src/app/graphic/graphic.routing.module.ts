import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ExampleComponent} from './example.component';

const routes: Routes = [
  {
    path: 'graphic',
    component: ExampleComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class GraphicRoutingModule {
}
