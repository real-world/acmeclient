import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {baseURL} from '../../shared/baseUrl';
import {BehaviorSubject} from 'rxjs';
import {DwTrainingGraphReport} from '../../shared/employeeTrainingGraph';

@Injectable()
export class RepoService {
  dataChange: BehaviorSubject<DwTrainingGraphReport[]> = new BehaviorSubject<DwTrainingGraphReport[]>([]);
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): DwTrainingGraphReport[] {
    return this.dataChange.value;
  }

  getReport() {
    return this.http.get<DwTrainingGraphReport[]>(baseURL + 'employeeTrainingGraph');

    // this.http.get<DwTrainingGraphReport[]>(baseURL + 'employeeTrainingGraph').subscribe(data => {
    //     this.dataChange.next(data);
    //     console.log(data);
    //   },
    //   (error: HttpErrorResponse) => {
    //     console.log(error.name + ' ' + error.message);
    //   });
  }
}

