import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TrainingService} from '../services/training.service';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/table';
import {BehaviorSubject, Observable} from 'rxjs';
import {AddTrainingComponent} from './dialog/add-training/add-training.component';
import {Training} from '../shared/training';
import {DeleteTrainingComponent} from './dialog/delete-training/delete-training.component';
import {EditTrainingComponent} from './dialog/edit-training/edit-training.component';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
  providers: [TrainingService]
})
export class TrainingComponent implements OnInit {
  displayedColumns = ['id', 'code', 'name', 'instructor', 'area', 'actions'];
  dataBase: TrainingService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public trainingService: TrainingService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;
  ngOnInit() {
    this.loadData();
  }
  refresh() {
    this.loadData();
  }
  addNew(training: Training) {
    const dialogRef = this.dialog.open(AddTrainingComponent, {
      data: {training: training}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*After dialog is closed we're doing frontend updates
        For add we're just pushing a new row inside DataService*/
        this.dataBase.dataChange.value.push(this.trainingService.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }

  startEdit(i: number, id: number, code: string, name: string, instructor: string, area: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(EditTrainingComponent, {
      data: {
        id: id, code: code, name: name, instructor: instructor, area: area
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*When using an edit things are little different,
        firstly we find record inside DataService by id*/
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*Then you update that record using data from dialogData
         (values you enetered)*/
        this.dataBase.dataChange.value[foundIndex] = this.trainingService.getDialogData();
        // And lastly refresh table
        this.loadData();
        this.refreshTable();
      }
    });
  }

  deleteItem(i: number, id: number, code: string, name: string, instructor: string, area: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteTrainingComponent, {
      data: {
        id: id, code: code, name: name, instructor: instructor, area: area
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*For delete we use splice in order to remove
        single object from DataService*/
        this.dataBase.dataChange.value.splice(foundIndex, 1);
        this.loadData();
        this.refreshTable();
      }
    });
  }
  /*If you don't need a filter or a pagination this can be simplified,
  you just use code from else block*/
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }
  public loadData() {
    this.dataBase = new TrainingService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}
export class ExampleDataSource extends DataSource<Training> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Training[] = [];
  renderedData: Training[] = [];

  constructor(public _trainingData: TrainingService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }
  /** Connect function called by the table to retrieve one
   stream containing the data to render. */
  connect(): Observable<Training[]> {
    // Listen for any changes in the base data,
    // sorting, filtering, or pagination
    const displayDataChanges = [
      this._trainingData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._trainingData.getAllTraininies();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._trainingData.data.slice().filter((training: Training) => {
        if (training !== undefined) {
          const searchStr = (training.id + training.code + training.name + training.instructor + training.area).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }
  /** Returns a sorted copy of the database data. */
  sortData(data: Training[]): Training[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'code':
          [propertyA, propertyB] = [a.code, b.code];
          break;
        case 'name':
          [propertyA, propertyB] = [a.name, b.name];
          break;
        case 'instructor':
          [propertyA, propertyB] = [a.instructor, b.instructor];
          break;
        case 'area':
          [propertyA, propertyB] = [a.area, b.area];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}
