import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {TrainingService} from '../../../services/training.service';
import {AreaService} from '../../../services/area.service';


@Component({
  selector: 'app-edit-training',
  templateUrl: './edit-training.component.html',
  styleUrls: ['./edit-training.component.scss']
})
export class EditTrainingComponent implements OnInit {
  areasTraining = [
    {value: 1, viewValue: 'Electricity'},
    {value: 2, viewValue: 'Heavy Work'},
    {value: 3, viewValue: 'Fine Work'},
    {value: 4, viewValue: 'Roofed'},
    {value: 5, viewValue: 'Fake Roof'},
    {value: 6, viewValue: 'Installation of Gas'},
    {value: 7, viewValue: 'Flooring'},
    {value: 8, viewValue: 'Inspection'},
    {value: 9, viewValue: 'Work Supervision'},
    {value: 10, viewValue: 'Hidrosanitary'},
    {value: 11, viewValue: 'Modified'}
  ];
  constructor(public dialogRef: MatDialogRef<EditTrainingComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public trainingService: TrainingService, public dataArea: AreaService) {
  this.dataArea.getAllAreas();
  }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.trainingService.updateTraining(this.data);
  }

  ngOnInit() {
  }

}
