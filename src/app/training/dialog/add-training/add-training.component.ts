import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Training} from '../../../shared/training';
import {TrainingService} from '../../../services/training.service';
import {AreaService} from '../../../services/area.service';
import {Area} from '../../../shared/area';

@Component({
  selector: 'app-add-training',
  templateUrl: './add-training.component.html',
  styleUrls: ['./add-training.component.scss']
})
export class AddTrainingComponent implements OnInit {

  areasTraining = [
    {value: 1, viewValue: 'Electricity'},
    {value: 2, viewValue: 'Heavy Work'},
    {value: 3, viewValue: 'Fine Work'},
    {value: 4, viewValue: 'Roofed'},
    {value: 5, viewValue: 'Fake Roof'},
    {value: 6, viewValue: 'Installation of Gas'},
    {value: 7, viewValue: 'Flooring'},
    {value: 8, viewValue: 'Inspection'},
    {value: 9, viewValue: 'Work Supervision'},
    {value: 10, viewValue: 'Hidrosanitary'},
    {value: 11, viewValue: 'Modified'}
  ];
  constructor(public dialogRef: MatDialogRef<AddTrainingComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Training,
              @Inject(MAT_DIALOG_DATA) public dataArea: Area,
              public trainingService: TrainingService, public areaService: AreaService) { }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.trainingService.addTraining(this.data);
  }

  ngOnInit() {
    this.areaService.getAllAreas();
  }

}
