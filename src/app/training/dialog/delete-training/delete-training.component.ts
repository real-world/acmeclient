import {Component, Inject, OnInit} from '@angular/core';
import {DeleteAreaComponent} from '../../../area/dialog/delete-area/delete-area.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {TrainingService} from '../../../services/training.service';

@Component({
  selector: 'app-delete-training',
  templateUrl: './delete-training.component.html',
  styleUrls: ['./delete-training.component.scss']
})
export class DeleteTrainingComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteAreaComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public trainingService: TrainingService) {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.trainingService.deleteTraining(this.data.id);
  }
  ngOnInit() {
  }

}
