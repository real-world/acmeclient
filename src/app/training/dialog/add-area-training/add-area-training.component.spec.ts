import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAreaTrainingComponent } from './add-area-training.component';

describe('AddAreaTrainingComponent', () => {
  let component: AddAreaTrainingComponent;
  let fixture: ComponentFixture<AddAreaTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAreaTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAreaTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
