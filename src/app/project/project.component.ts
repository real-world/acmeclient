import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {AddProjectComponent} from './AddProject/add/add.component';
import {EditProjectComponent} from './AddProject/edit/edit.component';
import {DeleteProjectComponent} from './AddProject/delete/delete.component';
import {BehaviorSubject, Observable} from 'rxjs';
import {DataSource} from '@angular/cdk/table';
import {ProjectService} from '../services/project.service';
import {Project} from '../shared/project';
import {AddItemComponent} from '../employee/dialog/add-item/add-item.component';
import {AddAreaToProjectComponent} from './AddProject/addAreaToProject/addAreaToProject.component';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
  providers: [ProjectService]
})
export class ProjectComponent implements OnInit {
  displayedColumns = ['id', 'name', 'description', 'date_start', 'date_end', 'area', 'actions'];
  dataBase: ProjectService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public projectService: ProjectService) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  addNew(project: Project) {
    const dialogRef = this.dialog.open(AddProjectComponent, {
      data: {project: project}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.dataBase.dataChange.value.push(this.projectService.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }

  startEdit(i: number, id: number, name: string, description: string, date_start: string, date_end: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(EditProjectComponent, {
      data: {
        id: id, name: name, description: description, date_start: date_start, date_end: date_end
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        this.dataBase.dataChange.value[foundIndex] = this.projectService.getDialogData();
        this.loadData();
        this.refreshTable();
      }
    });
  }

  addAreaToProject(id) {

    const dialogRef = this.dialog.open(AddAreaToProjectComponent, {
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        this.dataBase.dataChange.value[foundIndex] = this.projectService.getDialogData();
        this.loadData();
        this.refreshTable();
      }
    });
  }

  deleteProject(i: number, id: number, name: string, description: string, date_start: string, date_end: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteProjectComponent, {
      data: {
        id: id, name: name, description: description, date_start: date_start, date_end: date_end
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        this.dataBase.dataChange.value.splice(foundIndex, 1);
        this.loadData();
        this.refreshTable();
      }
    });
  }
  private refreshTable() {
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

  public loadData() {
    this.dataBase = new ProjectService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          console.log(this.dataSource);
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}

export class ExampleDataSource extends DataSource<Project> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Project[] = [];
  renderedData: Project[] = [];

  constructor(public _projectData: ProjectService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
  }

  connect(): Observable<Project[]> {
    const displayDataChanges = [
      this._projectData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    console.log('beforeeeee');
    this._projectData.getAllProjects();

    return Observable.merge(...displayDataChanges).map(() => {
      this.filteredData = this._projectData.data.slice().filter((project: Project) => {
        if (project !== undefined) {
          const searchStr = (project.id + project.name + project.description + project.date_start + project.date_end).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      const sortedData = this.sortData(this.filteredData.slice());

      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
     });
  }

  disconnect(){
  }

  sortData(data: Project[]): Project[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'name':
          [propertyA, propertyB] = [a.name, b.name];
          break;
        case 'description':
          [propertyA, propertyB] = [a.description, b.description];
          break;
        case 'date_start':
          [propertyA, propertyB] = [a.date_start, b.date_start];
          break;
        case 'date_end':
          [propertyA, propertyB] = [a.date_end, b.date_end];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}

















