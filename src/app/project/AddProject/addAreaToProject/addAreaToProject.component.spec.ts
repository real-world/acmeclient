import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddAreaToProjectComponent} from './addAreaToProject.component';

describe('AddAreaToProjectComponent', () => {
  let component: AddAreaToProjectComponent;
  let fixture: ComponentFixture<AddAreaToProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddAreaToProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAreaToProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
