import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AreaService} from '../../../services/area.service';
import {ProjectAreaService} from '../../../services/projectArea.service';

@Component({
  selector: 'app-add-area',
  templateUrl: './addAreaToProject.component.html',
  styleUrls: ['./addAreaToProject.component.scss']
})
export class AddAreaToProjectComponent implements OnInit {
  dataRead: any ;
  idArea: number;
  estado: string;
  constructor(public dialogRef: MatDialogRef<AddAreaToProjectComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public areaService: AreaService,
              public projectAreaService: ProjectAreaService) {
  this.areaService.getAllAreasToProject().subscribe(result => {
      this.dataRead = result;
      console.log(this.dataRead );
    },
    (error) => {
      console.log(error.name + ' ' + error.message);
    });
  }

  AddAreaProject() {
    let projectAreaModel = {id: 0, area: {id: this.idArea}, project: {id: this.data.id}, estado: this.estado};
    this.projectAreaService.addProjectArea(projectAreaModel);
    console.log(projectAreaModel);
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
