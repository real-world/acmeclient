import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Project} from '../../../shared/project';
import {ProjectService} from '../../../services/project.service';
import {FormControl, Validators} from '@angular/forms';
import {Component, Inject, OnInit} from '@angular/core';





@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddProjectComponent implements OnInit {

  date_start: string;
  date_end: string;
  constructor(public dialogRef: MatDialogRef<AddProjectComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Project,
              public projectService: ProjectService) {
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
        '';
  }

  submit() {
    //
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.projectService.addProject(this.data);
  }

  ngOnInit() {
  }

}
