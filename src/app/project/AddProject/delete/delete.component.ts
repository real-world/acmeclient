import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProjectService} from '../../../services/project.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteProjectComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteProjectComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public projectService: ProjectService) {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.projectService.deleteProject(this.data.id);
  }

  ngOnInit() {
  }

}
