import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {ProjectService} from '../../../services/project.service';
import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditProjectComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditProjectComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public projectService: ProjectService) {
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
        '';
  }

  submit() {
    //
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.projectService.updateProject(this.data);
  }

  ngOnInit() {
  }

}

