import {Component, OnInit} from '@angular/core';
import {Incident} from '../shared/incident';
import {IncidentService} from '../services/incident.service';

@Component({
  selector: 'app-accident',
  templateUrl: './accident.component.html',
  styleUrls: ['./accident.component.scss']
})
export class AccidentComponent implements OnInit {
  incident: Incident;

  constructor(private incidentService: IncidentService) {
  }

  ngOnInit() {
    this.incident = this.incidentService.getFeaturedIncident();
  }

}
