import { Component, OnInit } from '@angular/core';
import {ChartService} from '../services/chart.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],

})
export class HomeComponent implements OnInit {

  constructor(public chartService: ChartService) { }

  ngOnInit() {
    this.chartService.loadDatas();
  }

}
