import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AreaService} from '../services/area.service';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Area} from '../shared/area';
import {AddAreaComponent} from './dialog/add-area/add-area.component';
import {EditAreaComponent} from './dialog/edit-area/edit-area.component';
import {DeleteAreaComponent} from './dialog/delete-area/delete-area.component';
import {BehaviorSubject, Observable} from 'rxjs';
import {DataSource} from '@angular/cdk/table';
//import { Chart } from 'chart.js';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss'],
  providers: [AreaService]
})
export class AreaComponent implements OnInit {
  displayedColumns = ['id', 'code', 'name', 'actions'];
  dataBase: AreaService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;
  //chart = [];

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public areaService: AreaService) {  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;
  ngOnInit() {
    this.loadData();
  }
  refresh() {
    this.loadData();
  }

  addNew(area: Area) {
    const dialogRef = this.dialog.open(AddAreaComponent, {
      data: {area: area}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*After dialog is closed we're doing frontend updates
        For add we're just pushing a new row inside DataService*/
        this.dataBase.dataChange.value.push(this.areaService.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }
  startEdit(i: number, id: number, code: string, name: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(EditAreaComponent, {
      data: {
        id: id, code: code, name: name
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*When using an edit things are little different,
        firstly we find record inside DataService by id*/
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*Then you update that record using data from dialogData
        (values you enetered)*/
        this.dataBase.dataChange.value[foundIndex] = this.areaService.getDialogData();
        // And lastly refresh table
        this.loadData();
        this.refreshTable();
      }
    });
  }
  deleteItem(i: number, id: number, code: string, name: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteAreaComponent, {
      data: {
        id: id, code: code, name: name
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*For delete we use splice in order to remove
        single object from DataService*/
        this.dataBase.dataChange.value.splice(foundIndex, 1);
        this.loadData();
        this.refreshTable();
      }
    });
  }
 /* If you don't need a filter or a pagination this can be simplified,
  you just use code from else block*/
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }
  public loadData() {
    this.dataBase = new AreaService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  //   //Chart
  //   let quantities = this.dataSource.map(dataSource => dataSource.countEnventuality);
  //   let fullDates = this.dataSource.map(dataSource => dataSource.employeeName);
  //
  //   this.chart = new Chart('canvas', {
  //     type: 'line',
  //     data: {
  //              labels: fullDates,
  //     datasets: [
  //     {
  //                  data: quantities,
  //     borderColor: '#3cba9f',
  //     fill: false
  // }
  // ]
  // },
  //   options: {
  //     legend: {
  //       display: false
  //     },
  //     scales: {
  //       xAxes: [{
  //         display: true
  //       }],
  //         yAxes: [{
  //         display: true
  //       }]
  //     }
  //   }
  // });
  }
}
export class ExampleDataSource extends DataSource<Area> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Area[] = [];
  renderedData: Area[] = [];

  constructor(public _areaData: AreaService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }
  /*Connect function called by the table to retrieve one
  stream containing the data to render.*/
  connect(): Observable<Area[]> {
    // Listen for any changes in the base data,
    // sorting, filtering, or pagination
    const displayDataChanges = [
      this._areaData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    console.log('before');
    this._areaData.getAllAreas();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._areaData.data.slice().filter((area: Area) => {
        if (area !== undefined) {
          const searchStr = (area.id + area.code + area.name).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }
  /*Returns a sorted copy of the database data.*/
  sortData(data: Area[]): Area[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'code':
          [propertyA, propertyB] = [a.code, b.code];
          break;
        case 'name':
          [propertyA, propertyB] = [a.name, b.name];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}

