import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AreaService} from '../../../services/area.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-edit-area',
  templateUrl: './edit-area.component.html',
  styleUrls: ['./edit-area.component.scss']
})
export class EditAreaComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditAreaComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public areaService: AreaService) {
  }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    console.log(this.data);
    this.areaService.updateArea(this.data);
  }

  ngOnInit() {
  }

}
