import {Component, Inject, OnInit} from '@angular/core';
import {Area} from '../../../shared/area';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AreaService} from '../../../services/area.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-area',
  templateUrl: './add-area.component.html',
  styleUrls: ['./add-area.component.scss']
})
export class AddAreaComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddAreaComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Area,
              public areaService: AreaService) { }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.areaService.addArea(this.data);
  }

  ngOnInit() {
  }

}
