import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AreaService} from '../../../services/area.service';

@Component({
  selector: 'app-delete-area',
  templateUrl: './delete-area.component.html',
  styleUrls: ['./delete-area.component.scss']
})
export class DeleteAreaComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteAreaComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public areaService: AreaService) {
  }
  onCancelClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.areaService.deleteArea(this.data.id);
  }

  ngOnInit() {
  }
}
