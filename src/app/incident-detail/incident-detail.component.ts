import {Component, OnInit} from '@angular/core';
import {Incident} from '../shared/incident';
import {IncidentService} from '../services/incident.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-incident-detail',
  templateUrl: './incident-detail.component.html',
  styleUrls: ['./incident-detail.component.scss']
})
export class IncidentDetailComponent implements OnInit {
  incident: Incident;

  constructor(private incidentService: IncidentService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.params['id'];
    this.incident = this.incidentService.getIncident(id);
  }

  goBack(): void {
    this.location.back();
  }

}
