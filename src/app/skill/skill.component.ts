import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SkillService} from '../services/skill.service';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Skill} from '../shared/skill';
import {AddSkillComponent} from './dialog/add-skill/add-skill.component';
import {EditSkillComponent} from './dialog/edit-skill/edit-skill.component';
import {DeleteSkillComponent} from './dialog/delete-skill/delete-skill.component';
import {DataSource} from '@angular/cdk/table';
import {BehaviorSubject, Observable} from 'rxjs';

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss'],
  providers: [SkillService]
})
export class SkillComponent implements OnInit {
  displayedColumns = ['id', 'code', 'description', 'actions'];
  dataBase: SkillService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public skillService: SkillService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;
  ngOnInit() {
    this.loadData();
  }
  refresh() {
    this.loadData();
  }
  addNew(skill: Skill) {
    const dialogRef = this.dialog.open(AddSkillComponent, {
      data: {skill: skill}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*After dialog is closed we're doing frontend updates
        For add we're just pushing a new row inside DataService*/
        this.dataBase.dataChange.value.push(this.skillService.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }
  startEdit(i: number, id: number, code: string, description: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(EditSkillComponent, {
      data: {
        id: id, code: code, description: description
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*When using an edit things are little different,
        firstly we find record inside DataService by id*/
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*Then you update that record using data from dialogData
         (values you enetered)*/
        this.dataBase.dataChange.value[foundIndex] = this.skillService.getDialogData();
        // And lastly refresh table
        this.loadData();
        this.refreshTable();
      }
    });
  }
  deleteItem(i: number, id: number, code: string, description: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteSkillComponent, {
      data: {
        id: id, code: code, description: description
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*For delete we use splice in order to remove
        single object from DataService*/
        this.dataBase.dataChange.value.splice(foundIndex, 1);
        this.loadData();
        this.refreshTable();
      }
    });
  }
  /*If you don't need a filter or a pagination this can be simplified,
  you just use code from else block*/
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }
  public loadData() {
    this.dataBase = new SkillService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}
export class ExampleDataSource extends DataSource<Skill> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Skill[] = [];
  renderedData: Skill[] = [];

  constructor(public _skillData: SkillService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }
  /** Connect function called by the table to retrieve one
   stream containing the data to render. */
  connect(): Observable<Skill[]> {
    // Listen for any changes in the base data,
    // sorting, filtering, or pagination
    const displayDataChanges = [
      this._skillData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    console.log('before');
    this._skillData.getAllSkills();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._skillData.data.slice().filter((skill: Skill) => {
        if (skill !== undefined) {
          const searchStr = (skill.id + skill.code + skill.description).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }
  /** Returns a sorted copy of the database data. */
  sortData(data: Skill[]): Skill[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'code':
          [propertyA, propertyB] = [a.code, b.code];
          break;
        case 'description':
          [propertyA, propertyB] = [a.description, b.description];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}
