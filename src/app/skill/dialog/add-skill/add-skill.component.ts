import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SkillService} from '../../../services/skill.service';
import {Skill} from '../../../shared/skill';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.scss']
})
export class AddSkillComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddSkillComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Skill,
              public skillService: SkillService) { }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.skillService.addSkill(this.data);
  }

  ngOnInit() {
  }

}
