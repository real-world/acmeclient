import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SkillService} from '../../../services/skill.service';

@Component({
  selector: 'app-edit-skill',
  templateUrl: './edit-skill.component.html',
  styleUrls: ['./edit-skill.component.scss']
})
export class EditSkillComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditSkillComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public skillService: SkillService) {
  }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.skillService.updateSkill(this.data);
  }

  ngOnInit() {
  }

}
