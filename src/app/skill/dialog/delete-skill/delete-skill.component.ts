import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SkillService} from '../../../services/skill.service';

@Component({
  selector: 'app-delete-skill',
  templateUrl: './delete-skill.component.html',
  styleUrls: ['./delete-skill.component.scss']
})
export class DeleteSkillComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteSkillComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public skillService: SkillService) { }

  onCancelClick(): void {
  this.dialogRef.close();
}

confirmDelete(): void {
  this.skillService.deleteSkill(this.data.id);
}
  ngOnInit() {
  }

}
