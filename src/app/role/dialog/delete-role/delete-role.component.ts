import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {RoleService} from '../../../services/role.service';

@Component({
  selector: 'app-delete-role',
  templateUrl: './delete-role.component.html',
  styleUrls: ['./delete-role.component.scss']
})
export class DeleteRoleComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteRoleComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public roleService: RoleService) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.roleService.deleteRole(this.data.id);
  }

  ngOnInit() {
  }
}
