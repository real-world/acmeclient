import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyRuleComponent } from './safety-rule.component';

describe('SafetyRuleComponent', () => {
  let component: SafetyRuleComponent;
  let fixture: ComponentFixture<SafetyRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
