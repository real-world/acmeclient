import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SafetyRuleService} from '../services/safety-rule.service';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {AddSafetyRuleComponent} from './dialog/add-safety-rule/add-safety-rule.component';
import {SafetyRule} from '../shared/safetyRule';
import {EditSafetyRuleComponent} from './dialog/edit-safety-rule/edit-safety-rule.component';
import {DeleteSafetyRuleComponent} from './dialog/delete-safety-rule/delete-safety-rule.component';
import {BehaviorSubject, Observable} from 'rxjs';
import {DataSource} from '@angular/cdk/table';

@Component({
  selector: 'app-safety-rule',
  templateUrl: './safety-rule.component.html',
  styleUrls: ['./safety-rule.component.scss'],
  providers: [SafetyRuleService]
})
export class SafetyRuleComponent implements OnInit {
  displayedColumns = ['id', 'policyCode', 'policyName', 'accomplishment', 'actions'];
  dataBase: SafetyRuleService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public safetyRuleService: SafetyRuleService) {
   }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;
  ngOnInit() {
    this.loadData();
  }
  refresh() {
    this.loadData();
  }
  addNew(safetyRule: SafetyRule) {
    const dialogRef = this.dialog.open(AddSafetyRuleComponent, {
      data: {safetyRule: safetyRule}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*After dialog is closed we're doing frontend updates
        For add we're just pushing a new row inside DataService*/
        this.dataBase.dataChange.value.push(this.safetyRuleService.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }
  startEdit(i: number, id: number, policyCode: string, policyName: string, complianceParameter: number, complianceMetric: number) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(EditSafetyRuleComponent, {
      data: {
        id: id, policyCode: policyCode, policyName: policyName, complianceParameter: complianceParameter, complianceMetric: complianceMetric
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*When using an edit things are little different,
        firstly we find record inside DataService by id*/
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*Then you update that record using data from dialogData
         (values you enetered)*/
        this.dataBase.dataChange.value[foundIndex] = this.safetyRuleService.getDialogData();
        // And lastly refresh table
        this.refreshTable();
      }
    });
  }
  deleteItem(i: number, id: number, policyCode: string, policyName: string, complianceParameter: number, complianceMetric: number) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteSafetyRuleComponent, {
      data: {
        id: id, policyCode: policyCode, policyName: policyName
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.dataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*For delete we use splice in order to remove
        single object from DataService*/
        this.dataBase.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
      }
    });
  }
  /*If you don't need a filter or a pagination this can be simplified,
   you just use code from else block*/
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }
  public loadData() {
    this.dataBase = new SafetyRuleService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}
export class ExampleDataSource extends DataSource<SafetyRule> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: SafetyRule[] = [];
  renderedData: SafetyRule[] = [];

  constructor(public _safetyRuleData: SafetyRuleService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }
  /** Connect function called by the table to retrieve one
   stream containing the data to render. */
  connect(): Observable<SafetyRule[]> {
    // Listen for any changes in the base data,
    // sorting, filtering, or pagination
    const displayDataChanges = [
      this._safetyRuleData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    console.log('before');
    this._safetyRuleData.getAllSafetyRules();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._safetyRuleData.data.slice().filter((safetyRule: SafetyRule) => {
        if (safetyRule !== undefined) {
          const searchStr = (safetyRule.id + safetyRule.policyCode + safetyRule.policyName + safetyRule.complianceParameter).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }
  /** Returns a sorted copy of the database data. */
  sortData(data: SafetyRule[]): SafetyRule[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'code':
          [propertyA, propertyB] = [a.policyCode, b.policyCode];
          break;
        case 'name':
          [propertyA, propertyB] = [a.policyName, b.policyName];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}
