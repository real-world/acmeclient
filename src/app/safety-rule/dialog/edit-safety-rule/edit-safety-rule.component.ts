import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SafetyRuleService} from '../../../services/safety-rule.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-edit-safety-rule',
  templateUrl: './edit-safety-rule.component.html',
  styleUrls: ['./edit-safety-rule.component.scss']
})
export class EditSafetyRuleComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditSafetyRuleComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public safetyRuleService: SafetyRuleService) {
  }
  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.safetyRuleService.updateSafetyRule(this.data);
  }

  ngOnInit() {
  }

}
