import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSafetyRuleComponent } from './edit-safety-rule.component';

describe('EditSafetyRuleComponent', () => {
  let component: EditSafetyRuleComponent;
  let fixture: ComponentFixture<EditSafetyRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSafetyRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSafetyRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
