import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSafetyRuleComponent } from './delete-safety-rule.component';

describe('DeleteSafetyRuleComponent', () => {
  let component: DeleteSafetyRuleComponent;
  let fixture: ComponentFixture<DeleteSafetyRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteSafetyRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSafetyRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
