import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SafetyRuleService} from '../../../services/safety-rule.service';

@Component({
  selector: 'app-delete-safety-rule',
  templateUrl: './delete-safety-rule.component.html',
  styleUrls: ['./delete-safety-rule.component.scss']
})
export class DeleteSafetyRuleComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteSafetyRuleComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public safetyRuleService: SafetyRuleService) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.safetyRuleService.deleteSafetyRule(this.data.id);
  }

  ngOnInit() {
  }

}
