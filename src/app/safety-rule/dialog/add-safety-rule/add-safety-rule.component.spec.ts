import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSafetyRuleComponent } from './add-safety-rule.component';

describe('AddSafetyRuleComponent', () => {
  let component: AddSafetyRuleComponent;
  let fixture: ComponentFixture<AddSafetyRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSafetyRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSafetyRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
