import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SafetyRuleService} from '../../../services/safety-rule.service';
import {SafetyRule} from '../../../shared/safetyRule';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-safety-rule',
  templateUrl: './add-safety-rule.component.html',
  styleUrls: ['./add-safety-rule.component.scss']
})
export class AddSafetyRuleComponent implements OnInit {

  namePolicies = [
    {value: 'ADIESTRAMIENTO EN SEGURIDAD INDUSTRIAL', viewValue: 'ADIESTRAMIENTO EN SEGURIDAD INDUSTRIAL'},
    {value: 'DISTRIBUCION DE INFORMACION DE SEGURIDAD', viewValue: 'DISTRIBUCION DE INFORMACION DE SEGURIDAD'},
    {value: 'APLICACION DE PREMIACION A TRABAJADORES', viewValue: 'APLICACION DE PREMIACION A TRABAJADORES'},
    {value: 'APLIC. DE METODOLOGIAS DE CONSTRUCCION', viewValue: 'APLIC. DE METODOLOGIAS DE CONSTRUCCION'},
    {value: 'EVALUACION DE CONFIABILIDAD DE EQUIPOS', viewValue: 'EVALUACION DE CONFIABILIDAD DE EQUIPOS'},
    {value: 'USO DE EQUIPOS DE PROTECCION PERSONAL', viewValue: 'USO DE EQUIPOS DE PROTECCION PERSONAL'},
    {value: 'ASISTENCIA DE PRIMEROS AUXILIOS', viewValue: 'ASISTENCIA DE PRIMEROS AUXILIOS'},
    {value: 'SEGUIMIENTO DE EMPLEADOS CON INCIDENTES', viewValue: 'SEGUIMIENTO DE EMPLEADOS CON INCIDENTES'},
  ];
  complParams = [
    {value: '10', viewValue: '10 PERCENT'},
    {value: '20', viewValue: '20 PERCENT'},
    {value: '30', viewValue: '30 PERCENT'},
    {value: '40', viewValue: '40 PERCENT'},
    {value: '50', viewValue: '50 PERCENT'},
    {value: '60', viewValue: '60 PERCENT'},
    {value: '70', viewValue: '70 PERCENT'},
    {value: '80', viewValue: '80 PERCENT'},
    {value: '90', viewValue: '90 PERCENT'},
    {value: '100', viewValue: '100 PERCENT'},
  ];



  constructor(public dialogRef: MatDialogRef<AddSafetyRuleComponent>,
              @Inject(MAT_DIALOG_DATA) public data: SafetyRule,
              public safetyRuleService: SafetyRuleService) { }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.safetyRuleService.addSafetyRule(this.data);
  }

  ngOnInit() {
  }

}
