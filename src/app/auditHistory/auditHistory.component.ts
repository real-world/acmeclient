import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {BehaviorSubject, Observable} from 'rxjs';
import {DataSource} from '@angular/cdk/table';
import {HttpClient} from '@angular/common/http';
import {AuditHistoryService} from '../services/auditHistory.service';
import {AuditHistory} from '../shared/auditHistory';

@Component({
  selector: 'app-auditHistory',
  templateUrl: './auditHistory.component.html',
  styleUrls: ['./auditHistory.component.scss'],
  providers: [AuditHistoryService]
})
export class AuditHistoryComponent implements OnInit {
  displayedColumns = ['tableName', 'columnName', 'date', 'oldValue', 'newValue', 'modifiedBy', 'typeOperation'];
  dataBase: AuditHistoryService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public auditHistoryService: AuditHistoryService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }
  // If you don't need a filter or a pagination this can be simplified, you just use code from else block
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

  public loadData() {
    this.dataBase = new AuditHistoryService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.dataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}


export class ExampleDataSource extends DataSource<AuditHistory> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: AuditHistory[] = [];
  renderedData: AuditHistory[] = [];

  constructor(public _auditHistoryData: AuditHistoryService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<AuditHistory[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._auditHistoryData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    console.log('beforeeeee');
    this._auditHistoryData.getAllAuditHistorys();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._auditHistoryData.data.slice().filter((auditHistory: AuditHistory) => {
        if (auditHistory !== undefined) {
          const searchStr = (auditHistory.id + auditHistory.tableName + auditHistory.columnName + auditHistory.date
            + auditHistory.oldValue + auditHistory.newValue + auditHistory.modifiedBy + auditHistory.typeOperation).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }


  /** Returns a sorted copy of the database data. */
  sortData(data: AuditHistory[]): AuditHistory[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'name':
          [propertyA, propertyB] = [a.tableName, b.tableName];
          break;
        case 'instructor':
          [propertyA, propertyB] = [a.columnName, b.columnName];
          break;
        case 'firstName':
          [propertyA, propertyB] = [a.date, b.date];
          break;
        case 'lastName':
          [propertyA, propertyB] = [a.oldValue, b.oldValue];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}
