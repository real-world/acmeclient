import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AuditService} from '../../../services/audit.service';

@Component({
  selector: 'app-delete-audit',
  templateUrl: './delete-audit.component.html',
  styleUrls: ['./delete-audit.component.scss']
})
export class DeleteAuditComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteAuditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public auditService: AuditService) {
  }
  onCancelClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.auditService.deleteAudit(this.data.id);
  }

  ngOnInit() {
  }
}
