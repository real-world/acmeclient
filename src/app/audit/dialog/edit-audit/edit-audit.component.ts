import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AuditService} from '../../../services/audit.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-edit-audit',
  templateUrl: './edit-audit.component.html',
  styleUrls: ['./edit-audit.component.scss']
})
export class EditAuditComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditAuditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public auditService: AuditService) {
  }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.auditService.updateAudit(this.data);
  }

  ngOnInit() {
  }

}
