import {Component, Inject, OnInit} from '@angular/core';
import {Audit} from '../../../shared/audit';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AuditService} from '../../../services/audit.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-audit',
  templateUrl: './add-audit.component.html',
  styleUrls: ['./add-audit.component.scss']
})
export class AddAuditComponent implements OnInit {

  typeAudits = [
    {value: 'INTERNAL', viewValue: 'INTERNAL'},
    {value: 'EXTERNAL', viewValue: 'EXTERNAL'}
  ];

  periodicityAudits = [
    {value: 'MONTHLY', viewValue: 'MONTHLY'},
    {value: 'BIANNUAL', viewValue: 'BIANNUAL'},
    {value: 'ANNUAL', viewValue: 'ANNUAL'}
  ];





  constructor(public dialogRef: MatDialogRef<AddAuditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Audit,
              public auditService: AuditService) { }
  formControl = new FormControl('', [
    Validators.required
  ]);

  submit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.auditService.addAudit(this.data);
  }

  ngOnInit() {
  }

}
