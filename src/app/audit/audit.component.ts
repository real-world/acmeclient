import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuditService} from '../services/audit.service';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Audit} from '../shared/audit';
import {AddAuditComponent} from './dialog/add-audit/add-audit.component';
import {EditAuditComponent} from './dialog/edit-audit/edit-audit.component';
import {DeleteAuditComponent} from './dialog/delete-audit/delete-audit.component';
import {BehaviorSubject, Observable} from 'rxjs';
import {DataSource} from '@angular/cdk/table';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';


@Component({
  selector: 'app-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.scss'],
  providers: [AuditService]
})
export class AuditComponent implements OnInit {
  displayedColumns = ['id', 'auditName', 'auditCode', 'auditType', 'auditPeriodicity', 'auditScope', 'auditObjective', 'actions'];
  // dataBase: AuditService | null;
  // dataSource: ExampleDataSource | null;

  auditDataBase: AuditService | null;
  dataSource: AuditDataSource | null;
  index: number;
  id: number;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public auditService: AuditService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  addNew(audit: Audit) {
    const dialogRef = this.dialog.open(AddAuditComponent, {
      data: {audit: audit}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*After dialog is closed we're doing frontend updates
        For add we're just pushing a new row inside DataService*/
        this.auditDataBase.dataChange.value.push(this.auditService.getDialogData());
        this.loadData();
        this.refreshTable();
      }
    });
  }

  startEdit(i: number, id: number, auditName: string, auditCode: string, auditType: string, auditPeriodicity: string, auditScope: string, auditObjective: string, auditCriteria: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(EditAuditComponent, {
      data: {
        id: id,
        auditName: auditName,
        auditCode: auditCode,
        auditType: auditType,
        auditPeriodicity: auditPeriodicity,
        auditScope: auditScope,
        auditObjective: auditObjective,
        auditCriteria: auditCriteria
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        /*When using an edit things are little different,
        firstly we find record inside DataService by id*/
        const foundIndex = this.auditDataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*Then you update that record using data from dialogData
        (values you entered)*/
        this.auditDataBase.dataChange.value[foundIndex] = this.auditService.getDialogData();
        // And lastly refresh table
        this.loadData();
        this.refreshTable();
      }
    });
  }

  deleteItem(i: number, id: number, auditName: string, auditCode: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteAuditComponent, {
      data: {
        id: id, auditName: auditName, auditCode: auditCode
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.auditDataBase.dataChange.value.findIndex(x => x.id === this.id);
        /*For delete we use splice in order to remove
        single object from DataService*/
        this.auditDataBase.dataChange.value.splice(foundIndex, 1);
        this.loadData();
        this.refreshTable();
      }
    });
  }

  // addItem() {
  //   // this.index = i;
  //   console.log('adding item');
  //   const dialogRef = this.dialog.open(AddAuditComponent, {
  //     data: {}
  //   });
  //
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result === 1) {
  //       // When using an edit things are little different, firstly we find record inside DataService by id
  //       const foundIndex = this.auditDataBase.dataChange.value.findIndex(x => x.id === this.id);
  //       // Then you update that record using data from dialogData (values you enetered)
  //       this.auditDataBase.dataChange.value[foundIndex] = this.auditService.getDialogData();
  //       // And lastly refresh table
  //       this.loadData();
  //       this.refreshTable();
  //     }
  //   });
  // }
  /* If you don't need a filter or a pagination this can be simplified,
   you just use code from else block*/
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

  public loadData() {
    this.auditDataBase = new AuditService(this.httpClient);
    this.dataSource = new AuditDataSource(this.auditDataBase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}

export class AuditDataSource extends DataSource<Audit> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Audit[] = [];
  renderedData: Audit[] = [];

  constructor(public _auditData: AuditService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /*Connect function called by the table to retrieve one
  stream containing the data to render.*/
  connect(): Observable<Audit[]> {
    // Listen for any changes in the base data,
    // sorting, filtering, or pagination
    const displayDataChanges = [
      this._auditData.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    console.log('before');
    this._auditData.getAllAudits();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._auditData.data.slice().filter((audit: Audit) => {
        if (audit !== undefined) {
          const searchStr = (audit.id + audit.auditName + audit.auditCode).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }

  disconnect() {
  }

  /*Returns a sorted copy of the database data.*/
  sortData(data: Audit[]): Audit[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'auditName':
          [propertyA, propertyB] = [a.auditName, b.auditName];
          break;
        case 'auditCode':
          [propertyA, propertyB] = [a.auditCode, b.auditCode];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

}

